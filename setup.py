import setuptools

with open('requirements.txt', 'r') as f:
    install_requires = f.read().splitlines()

setuptools.setup(name='restic_ui',
                 packages=['restic_ui'],
                 install_requires=install_requires)
