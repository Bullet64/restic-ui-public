README
======

## About

This project is not related to [restic.net](https://restic.net)

My second Python3 PyQT5 project. Since version 1.3.0 I use PyQt6 for it.

The project is a UI for the CLI tool [restic](https://restic.net)

This project is quite far from perfect, but it helped me a lot to understand the world of Python3, VSCodium and git a little.

If it helps someone along the way as well, I'm glad. For questions and suggestions you can write me in my [forum](https://linux-nerds.org/category/54/restic-ui)

And now have fun with one of the best backup tools!

## Dependencies

* restic
* python >= 3.10
* pipenv
* git

## Functions Restic-UI

**The data of the backup database can be stored encrypted. For this purpose, there is a key file that can be stored in a safe place.**

Restic functions that are included in the UI

* init
* backup
* mount
* restore
* snapshots
* ls
* check
* unlock
* stats
* prune
* version
* migrate check
* migrate update

Restic REST-Backend Functions included

* init
* backup
* mount
* restore
* snapshots
* ls
* check
* unlock
* stats
* prune

Source: https://github.com/restic/rest-server

## Installation

### Install restic if not installed

    apt install git
    apt install restic
    restic self-update

### Clone Repository

    git clone https://gitlab.com/Bullet64/restic-ui-public.git

### Create env

    cd /home/USER/restic-ui-public

    We need last pip to install PyQt6
    python -m pip install -U pip
    pip install pipenv

    pipenv --python 3.10
    pipenv shell

### Install dependencies

    pipenv install PyQt6
    pipenv install cryptography

## Usage

    python3 src/restic_ui.py

## Known problems

    None at present

![main screen](/icons/Restic_UI_main_screen.png "Main Screen")

## Tested on

* Linux Mint Cinnamon 20.2 / 20.3 / 21
* Manjaro 21.3.7 Ruah
* Manjaro 22.2.0 Sikaris
* Windows Server 2019

with

* python >= 3.10
