""" Import Functions """


# ------ import for PyQt6 ------ #
from PyQt6.QtWidgets import (
    QLabel,
    QLineEdit,
    QWidget,
    QPushButton,
    QGridLayout,
    QTabWidget)

from PyQt6.QtGui import QIcon, QRegularExpressionValidator

from PyQt6.QtCore import Qt, pyqtSignal

from PyQt6 import QtCore

# ------ import functions ------ #
from functions import (
    msg_box_error,
    get_path,
    get_file_path)

# ------ import for file check ------ #
from pathlib import Path

from crypt import Crypt

import ini


###############################################
# Third Window / Settings
###############################################


class SettingsWindow(QWidget):
    """ Third Window / Settings """

    send_signal = pyqtSignal(str)

    def __init__(self):
        super().__init__()

        ###############################################
        # Restic Presets
        ###############################################

        def save_restic_presets():
            """ Restic Presets """

            # Restic Check Percent

            ini.settings.setValue('Restic_Check_Percent', str(self.input_restic_check_percent.text()))

            self.input_restic_check_percent.setText(ini.settings.value('Restic_Check_Percent'))

            # Restic Prune Keep Last

            ini.settings.setValue('Restic_Keep_Last', str(self.input_restic_keep_last.text()))

            self.input_restic_keep_last.setText(ini.settings.value('Restic_Keep_Last'))

            # Restic Prune Keep Monthly

            ini.settings.setValue('Restic_Keep_Monthly', str(self.input_restic_keep_monthly.text()))

            self.input_restic_keep_monthly.setText(ini.settings.value('Restic_Keep_Monthly'))

        ###############################################
        # Save UI Settings
        ###############################################

        def save_ui_settings():
            """ Save UI Settings """

            # Home Path

            ini.settings.setValue('Home_Path', str(self.input_home_path.text()))

            self.input_home_path.setText(ini.settings.value('Home_Path'))

            # Source Path

            ini.settings.setValue('Source_Path', str(self.input_source_path.text()))

            self.input_source_path.setText(ini.settings.value('Source_Path'))

            # Exclude List

            ini.settings.setValue('Exclude_List', str(self.input_exclude_list.text()))

            self.input_exclude_list.setText(ini.settings.value('Exclude_List'))

            # Key Path

            ini.settings.setValue('Key_Path', str(self.input_key_path.text()))

            self.input_key_path.setText(ini.settings.value('Key_Path'))

            # Restic DEV

            ini.settings.setValue('Restic_DEV', str(self.input_restic_dev.text()))

            self.input_restic_dev.setText(ini.settings.value('Restic_DEV'))

            if not (ini.settings.value('Restic_DEV')):

                self.send_signal.emit("Restic Version")

            else:

                self.send_signal.emit("Restic DEV Version")

        ###############################################
        # Set home path to save in file settings
        ###############################################

        def get_home_path():
            """ Set home path to save in file settings """

            path = get_path(self, 'Home_Path', 'Home Path')

            if not path:
                return False
            else:
                self.input_home_path.setText(path)

        ###############################################
        # Get source path to save in file settings
        ###############################################

        def get_source_path():
            """ Get source path to save in file settings """

            path = get_path(self, 'Source_Path', 'Source Path')

            if not path:
                return False
            else:
                self.input_source_path.setText(path)

        ###############################################
        # get_exclude_list
        ###############################################

        def get_exclude_list():
            """ get_exclude_list """

            path = get_file_path(self, 'Exclude_List', 'Exclude List')

            if not path[0]:
                return False
            else:
                self.input_exclude_list.setText(path[0])

        ###############################################
        # get_key_path
        ###############################################

        def get_key_path():
            """ get_key_path """

            path = get_path(self, 'Key_Path', 'Key Path')

            if not path:
                return False
            else:
                self.input_key_path.setText(path)

        def get_restic_dev():
            """ get restic_dev """

            path = get_file_path(self, 'Restic_DEV', 'Restic_DEV')
            # print("TEST", path)

            if not path[0]:
                return False
            else:
                self.input_restic_dev.setText(path[0])

        ###############################################
        # Functions for delete input field
        ###############################################

        def del_source():
            """ Functions for delete input field """
            self.input_source_path.clear()

        def del_home():
            """ Functions for delete input field """

            self.input_home_path.clear()
            ini.settings.remove('Home_Path')

        def del_exclude():
            """ Functions for delete input field """

            self.input_exclude_list.clear()

        def del_key():
            """ Functions for delete input field """

            self.input_key_path.clear()

        def del_restic_dev():
            """ Functions for delete input field """

            self.input_restic_dev.clear()

        ###############################################
        # createKey
        ###############################################

        def create_key():
            """ Set state for input, button, etc. and create the key"""

            # Key path set?
            path = ini.settings.value('Key_Path')

            if not path:
                msg_box_error(self, "Please set a key path on tab folder")
                return False
            else:
                ini.settings.setValue('Crypt', 1)
                ini.settings.setValue('Key', 1)

                self.pushbutton_create_key.setEnabled(False)
                self.pushbutton_reset_encryption.setEnabled(True)
                self.pushbutton_reset_encryption.setStyleSheet("background : red")

                # Generate key
                Crypt.write_key(path)

        def reset_encryption():
            """ Set state for input, button, etc. """

            ini.settings.setValue('Key', 0)
            ini.settings.setValue('Crypt', 0)
            ini.settings.setValue('Key_Path', "")

            self.pushbutton_create_key.setEnabled(True)
            self.pushbutton_reset_encryption.setEnabled(False)
            self.pushbutton_reset_encryption.setStyleSheet("")

            self.input_key_path.clear()

            # delete KEY_FILE & KEY_PATH Folder!
            rem_file = Path(ini.KEY_FILE)
            rem_file.unlink()

            rem_path = Path(ini.KEY_PATH)
            rem_path.rmdir()

        def input_path_settings_changed():
            """ input path settings changed """
            # print("changed")
            self.button_save_ui_settings.setEnabled(True)

        def input_restic_settings_changed():
            """ input restic settings changed """

            self.button_save_restic_presets.setEnabled(True)

        ###############################################
        # Window definition
        ###############################################

        self.title = 'Restic UI - Settings'
        self.setWindowModality(Qt.WindowModality.ApplicationModal)
        self.setWindowTitle(self.title)
        self.setMinimumSize(440, 250)
        self.setMaximumSize(440, 250)

        #####################
        # Button
        #####################

        self.button_save_ui_settings = QPushButton("", self)
        self.button_save_ui_settings.setIcon(QIcon.fromTheme(
            'document-save', QIcon(ini.CWD + "./icons/save")))
        self.button_save_ui_settings.clicked.connect(save_ui_settings)
        self.button_save_ui_settings.setMaximumWidth(100)

        self.button_save_restic_presets = QPushButton("", self)
        self.button_save_restic_presets.setIcon(QIcon.fromTheme(
            'document-save', QIcon(ini.CWD + "./icons/save")))
        self.button_save_restic_presets.clicked.connect(save_restic_presets)
        self.button_save_restic_presets.setMaximumWidth(50)

        # Get Home Path
        self.button3 = QPushButton("", self)
        self.button3.setIcon(QIcon.fromTheme(
            'document-open-folder', QIcon(ini.CWD + "./icons/folder-open")))
        self.button3.clicked.connect(get_home_path)

        # Get Source Path
        self.button4 = QPushButton("", self)
        self.button4.setIcon(QIcon.fromTheme(
            'document-open-folder', QIcon(ini.CWD + "./icons/folder-open")))
        self.button4.clicked.connect(get_source_path)

        # Get Exclude List
        self.button5 = QPushButton("", self)
        self.button5.setIcon(QIcon.fromTheme(
            'document-open-folder', QIcon(ini.CWD + "./icons/folder-open")))
        self.button5.clicked.connect(get_exclude_list)

        # Clear
        self.button6 = QPushButton("", self)
        self.button6.setIcon(QIcon.fromTheme(
            'edit-reset', QIcon(ini.CWD + "./icons/edit-clear")))
        self.button6.clicked.connect(del_source)

        # Clear
        self.button7 = QPushButton("", self)
        self.button7.setIcon(QIcon.fromTheme(
            'edit-reset', QIcon(ini.CWD + "./icons/edit-clear")))
        self.button7.clicked.connect(del_home)

        # Clear
        self.button8 = QPushButton("", self)
        self.button8.setIcon(QIcon.fromTheme(
            'edit-reset', QIcon(ini.CWD + "./icons/edit-clear")))
        self.button8.clicked.connect(del_exclude)

        # Get Home Path
        self.get_key_path = QPushButton("", self)
        self.get_key_path.setIcon(QIcon.fromTheme(
            'document-open-folder', QIcon(ini.CWD + "./icons/folder-open")))
        self.get_key_path.clicked.connect(get_key_path)

        # Clear
        self.button10 = QPushButton("", self)
        self.button10.setIcon(QIcon.fromTheme(
            'edit-reset', QIcon(ini.CWD + "./icons/edit-clear")))
        self.button10.clicked.connect(del_key)

        # Clear Restic DEV
        self.button11 = QPushButton("", self)
        self.button11.setIcon(QIcon.fromTheme(
            'edit-reset', QIcon(ini.CWD + "./icons/edit-clear")))
        self.button11.clicked.connect(del_restic_dev)

        # Get Restic DEV
        self.get_restic_dev2 = QPushButton("", self)
        self.get_restic_dev2.setIcon(QIcon.fromTheme(
            'document-open-folder', QIcon(ini.CWD + "./icons/folder-open")))
        self.get_restic_dev2.clicked.connect(get_restic_dev)

        #####################
        # Fonts etc.
        #####################

        # Create empty lines as spacers.
        self.label_3 = QLabel("Encrypt your 'backup_list.json' file.")
        self.label_5 = QLabel("Decrypt yor 'backup_list.json' file.")
        self.empty_line_1 = QLabel()
        self.label_7 = QLabel("Key Path")
        self.warning = QLabel("Please use the encryption function at your own risk.")
        self.warning.setStyleSheet("font-weight: bold ; font-size: 15px")
        self.empty_line_2 = QLabel()
        self.label_9 = QLabel("Restic Settings")
        self.label_9.setStyleSheet("font-weight: bold ; font-size: 15px")
        self.empty_line_3 = QLabel()

        #####################
        # Line Edits
        #####################

        self.input_home_path = QLineEdit(str(ini.settings.value('Home_Path')), self)
        self.input_home_path.setToolTip("Specify where your system store the backups.")

        self.input_source_path = QLineEdit(str(ini.settings.value('Source_Path')), self)
        self.input_source_path.setToolTip("Specify where your system looks for backup sources.")

        self.input_exclude_list = QLineEdit(str(ini.settings.value('Exclude_List')), self)
        self.input_exclude_list.setToolTip("Specify if you want to use an exclude list")

        self.input_key_path = QLineEdit(str(ini.settings.value('Key_Path')), self)
        self.input_key_path.setToolTip("Specify where your encryption key should be stored")

        self.input_restic_check_percent = QLineEdit(str(ini.settings.value('Restic_Check_Percent')), self)
        self.input_restic_check_percent.setMaximumWidth(26)
        self.input_restic_check_percent.setToolTip("Set how much data will be tested. (1 - 100%)")

        self.input_restic_keep_last = QLineEdit(str(ini.settings.value('Restic_Keep_Last')), self)
        self.input_restic_keep_last.setMaximumWidth(16)
        self.input_restic_keep_last.setToolTip("How many snapshots should be kept? (1 - 9)")

        self.input_restic_keep_monthly = QLineEdit(str(ini.settings.value('Restic_Keep_Monthly')), self)
        self.input_restic_keep_monthly.setMaximumWidth(25)
        self.input_restic_keep_monthly.setToolTip("How many snapshots of the last months should be kept? (1 - 12)")
        # self.input_restic_check_percent.setAlignment(Qt.AlignmentFlag.AlignRight)

        self.input_restic_dev = QLineEdit(str(ini.settings.value('Restic_DEV')), self)
        self.input_restic_dev.setToolTip("Specify where your restic DEV version is located.")

        self.input_home_path.setReadOnly(True)
        self.input_source_path.setReadOnly(True)
        self.input_exclude_list.setReadOnly(True)
        self.input_key_path.setReadOnly(True)

        #####################
        # RegEx
        #####################

        reg_ex = QtCore.QRegularExpression('^([1-9]|[1-9][0-9]|100)$')
        input_validator = QRegularExpressionValidator(reg_ex, self.input_restic_check_percent)
        self.input_restic_check_percent.setValidator(input_validator)

        reg_ex = QtCore.QRegularExpression('^[0-9]{1}$')
        input_validator = QRegularExpressionValidator(reg_ex, self.input_restic_keep_last)
        self.input_restic_keep_last.setValidator(input_validator)

        reg_ex = QtCore.QRegularExpression('^([1-9]|[1-1][0-2])$')
        input_validator = QRegularExpressionValidator(reg_ex, self.input_restic_keep_monthly)
        self.input_restic_keep_monthly.setValidator(input_validator)

        #####################
        # Check Boxes
        #####################

        self.pushbutton_create_key = QPushButton("Create Key!?")
        self.pushbutton_reset_encryption = QPushButton("Reset Encryption!")

        #####################
        # Layout
        #####################

        # Create layout
        self.layout2 = QGridLayout()

        self.tabs = QTabWidget()
        self.tab_encryption = QWidget()
        self.tab_path = QWidget()
        self.tab_restic = QWidget()

        self.tabs.addTab(self.tab_encryption, "Encryption")
        self.tabs.addTab(self.tab_path, "Path settings")
        self.tabs.addTab(self.tab_restic, "Restic settings")

        #####################
        # Tab Encryption
        #####################

        self.tab_encryption.layout = QGridLayout()

        self.tab_encryption.layout.addWidget(self.warning, 1, 0)

        self.tab_encryption.layout.addWidget(self.empty_line_2, 2, 0)

        self.tab_encryption.layout.addWidget(self.label_3, 3, 0)

        self.tab_encryption.layout.addWidget(self.pushbutton_create_key, 4, 0)
        self.pushbutton_create_key.clicked.connect(create_key)

        self.tab_encryption.layout.addWidget(self.label_5, 5, 0)

        self.tab_encryption.layout.addWidget(self.pushbutton_reset_encryption, 6, 0)
        self.pushbutton_reset_encryption.clicked.connect(reset_encryption)

        self.tab_encryption.setLayout(self.tab_encryption.layout)

        #####################
        # Tab Path
        #####################

        self.tab_path.layout = QGridLayout()

        self.tab_path.layout.addWidget(QLabel("Home Path"), 1, 0)
        self.tab_path.layout.addWidget(self.button3, 1, 3)
        self.tab_path.layout.addWidget(self.button7, 1, 2)
        self.tab_path.layout.addWidget(self.input_home_path, 1, 1)
        self.input_home_path.textChanged.connect(input_path_settings_changed)

        self.tab_path.layout.addWidget(QLabel("Source Path"), 2, 0)
        self.tab_path.layout.addWidget(self.button4, 2, 3)
        self.tab_path.layout.addWidget(self.button6, 2, 2)
        self.tab_path.layout.addWidget(self.input_source_path, 2, 1)
        self.input_source_path.textChanged.connect(input_path_settings_changed)

        self.tab_path.layout.addWidget(QLabel("Exclude List"), 3, 0)
        self.tab_path.layout.addWidget(self.button5, 3, 3)
        self.tab_path.layout.addWidget(self.button8, 3, 2)
        self.tab_path.layout.addWidget(self.input_exclude_list, 3, 1)
        self.input_exclude_list.textChanged.connect(input_path_settings_changed)

        self.tab_path.layout.addWidget(self.label_7, 4, 0)
        self.tab_path.layout.addWidget(self.get_key_path, 4, 3)
        self.tab_path.layout.addWidget(self.button10, 4, 2)
        self.tab_path.layout.addWidget(self.input_key_path, 4, 1)
        self.input_key_path.textChanged.connect(input_path_settings_changed)
        # self.input_key_path.textChanged.connect(changed_test)

        self.tab_path.layout.addWidget(QLabel("Restic DEV Version"), 5, 0)
        self.tab_path.layout.addWidget(self.get_restic_dev2, 5, 3)
        self.tab_path.layout.addWidget(self.button11, 5, 2)
        self.tab_path.layout.addWidget(self.input_restic_dev, 5, 1)
        self.input_restic_dev.textChanged.connect(input_path_settings_changed)

        self.tab_path.layout.addWidget(self.empty_line_1, 6, 0)

        self.tab_path.layout.addWidget(self.button_save_ui_settings, 7, 0)

        self.tab_path.setLayout(self.tab_path.layout)

        #####################
        # Tab Restic
        #####################

        self.tab_restic.layout = QGridLayout()

        self.tab_restic.layout.addWidget(self.label_9, 0, 0)

        self.tab_restic.layout.addWidget(QLabel("Check %"), 1, 0)
        self.tab_restic.layout.addWidget(self.empty_line_3, 1, 2)
        self.tab_restic.layout.addWidget(self.input_restic_check_percent, 1, 1)
        self.input_restic_check_percent.textChanged.connect(input_restic_settings_changed)

        self.tab_restic.layout.addWidget(QLabel("Prune Keep Last"), 2, 0)
        self.tab_restic.layout.addWidget(self.empty_line_3, 2, 2)
        self.tab_restic.layout.addWidget(self.input_restic_keep_last, 2, 1)
        self.input_restic_keep_last.textChanged.connect(input_restic_settings_changed)

        self.tab_restic.layout.addWidget(QLabel("Prune Keep Monthly"), 3, 0)
        self.tab_restic.layout.addWidget(self.empty_line_3, 3, 2)
        self.tab_restic.layout.addWidget(self.input_restic_keep_monthly, 3, 1)
        self.input_restic_keep_monthly.textChanged.connect(input_restic_settings_changed)

        self.tab_restic.layout.addWidget(self.empty_line_1, 4, 0)

        self.tab_restic.layout.addWidget(self.button_save_restic_presets, 5, 0)

        self.tab_restic.setLayout(self.tab_restic.layout)

        ########

        self.layout2.addWidget(self.tabs)

        self.setLayout(self.layout2)

        self.button_save_ui_settings.setEnabled(False)
        self.button_save_restic_presets.setEnabled(False)

        # Here we read state from 'status_crypt'
        # print("Status", type(ini.settings.value("Crypt")))

        # Start
        # print("TYPE", type(ini.settings.value("Crypt")))
        if int(ini.settings.value("Crypt")) == 0:
            self.pushbutton_create_key.setEnabled(True)
            self.pushbutton_reset_encryption.setEnabled(False)

        # Key exists
        if int(ini.settings.value("Crypt")) == 1:
            self.pushbutton_create_key.setEnabled(False)
            self.pushbutton_reset_encryption.setEnabled(False)

        # Encrypted
        if int(ini.settings.value("Crypt")) == 2:
            self.pushbutton_create_key.setEnabled(False)
            self.pushbutton_reset_encryption.setEnabled(True)
            self.pushbutton_reset_encryption.setStyleSheet("background : red")
