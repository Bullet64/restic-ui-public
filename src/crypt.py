from PyQt6.QtCore import QSettings

# ------ import for file check ------ #
from pathlib import Path

# ------ import for crypt ------ #
from cryptography.fernet import Fernet

import ini

settings = QSettings("FrankMankel", "Restic_UI")

#####################
# Class Crypt
#####################


class Crypt():
    "Class to crypt/encrypt settings file"

    def __init__(self):
        pass

    def write_key(path):
        """
        Generates a key and save it into a file
        """

        KEYPATH = (f"{path}/crypt.key")

        # create crypt.key if not exists
        if Path(KEYPATH).exists():
            print("Error! Key file exists!?")
            # check!!
            return False

        else:
            key = Fernet.generate_key()

            with open(KEYPATH, "wb") as key_file:
                key_file.write(key)

    def load_key():
        """
        Loads the key from the current directory named `key.key`
        """
        KEY_PATH = settings.value("Key_Path")

        KEY_FILE = f"{KEY_PATH}/crypt.key"

        try:
            return open(KEY_FILE, "rb").read()
        except Exception as e:
            return -1

    def encrypt(filename, key):
        """
        Given a filename (str) and key (bytes), it encrypts the file and write it
        """
        f = Fernet(key)

        # check for 'backup_list.enc'
        if not Path(ini.CRYPT_FILE).exists():

            with open(ini.JSON_FILE, "rb") as file:
                # read all file data
                file_data = file.read()

            # encrypt data
            encrypted_data = f.encrypt(file_data)

            with open(ini.CRYPT_FILE, "wb") as file:
                file.write(encrypted_data)

            # delete 'backup_list.json'!
            rem_file = Path(ini.JSON_FILE)
            rem_file.unlink()

        else:
            print("Error")

        return "Prost"  # Rückgabe von Klasse

    def decrypt(filename, key):
        """
        Given a filename (str) and key (bytes), it decrypts the file and write it
        """
        f = Fernet(key)

        # check for 'backup_list.enc'
        if Path(ini.CRYPT_FILE).exists():

            with open(filename, "rb") as file:
                # read the encrypted data
                encrypted_data = file.read()

            # decrypt data
            decrypted_data = f.decrypt(encrypted_data)

            # write the original file
            ENCRYPT_PATH = f"{ini.USERHOME}/.restic_ui/backup_list.json"
            with open(ENCRYPT_PATH, "wb") as file:
                file.write(decrypted_data)

            # delete 'backup_list.enc'
            rem_file = Path(ini.CRYPT_FILE)
            rem_file.unlink()

        else:
            print("ERROR in decrypt")
