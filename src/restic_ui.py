#! /usr/bin/python3

""" Doc String """

# =============== Licence: GPL v3 ==========================================
#
# Author:
#   Frank Mankel <frank.mankel@gmail.com>
#

#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#

#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
# MA 02110-1301, USA.
#
# ==========================================================================


###############################################
# Import Functions
###############################################

# ------ import so we can use sys.exit ------ #
import sys

# ------ import so we can use os.name ------ #
import os

# ------ import so we can do time.sleep() in function restic_mount ------ #
import time

# ------ import for subprocess ------ #
import subprocess

# ------ import for json ------ #
import json

# ------ import for file check ------ #
from pathlib import Path, PurePath

from crypt import Crypt

# ------ import for PyQt6 ------ #
from PyQt6.QtWidgets import (
    QMainWindow,
    QToolBar,
    QApplication,
    QLabel,
    QVBoxLayout,
    QPlainTextEdit,
    QFileDialog,
    QInputDialog,
    QLineEdit,
    QWidget,
    QTextEdit,
    QDockWidget,
    QListWidget,
    QPushButton,
    QGridLayout,
    QTabWidget,
    QFormLayout,
    QCheckBox,
    QToolButton)

from PyQt6.QtGui import QIcon, QAction, QRegularExpressionValidator


from PyQt6.QtCore import (
    Qt,
    QSize,
    QObject,
    QThread,
    pyqtSlot,
    pyqtSignal)

from PyQt6 import QtCore


# ------ import functions ------ #
from functions import (
    msg_box,
    msg_box_about,
    msg_box_yes_no,
    msg_box_error,
    get_path,
    get_file_path,
    get_password)

from waitingspinnerwidget import QtWaitingSpinner

import ini

from settings import SettingsWindow

print(sys.version)

print(sys.path)

###############################################
# Check for restic
###############################################

# ------ check for restic ------ #
try:
    subprocess.run(['restic'], check=True, stdout=subprocess.PIPE,
                   universal_newlines=True)
except OSError as e:
    if e.errno == 2:
        sys.exit("Please install restic, **sudo apt install restic**")


###############################################
# Worker for restic_stats
###############################################


class WorkerResticStats(QObject):
    """ Worker Class for Rest function restic_stats """

    stats_finished = pyqtSignal(str)
    stats_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        """ Worker for restic_stats """

        restic = ResticDev.get(self)

        # We get the current row from the List widget
        row = ActiveRow.get(self)

        result = None
        try:
            match_list = [backup_data[row].rest, bool(backup_data[row].password)]

            match match_list:

                # backup_data[row].rest, bool(backup_data[row].password)
                case ["0", False]:
                    # Restic function
                    args = [restic,
                            '-r',
                            backup_data[row].repository,
                            'stats']

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw

                # backup_data[row].rest, bool(backup_data[row].password)
                case ["0", True]:
                    # Restic function
                    args = [restic,
                            '-r',
                            backup_data[row].repository,
                            'stats']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

                # backup_data[row].rest, bool(backup_data[row].password)
                case ["1", False]:
                    # Restic function
                    args = [restic,
                            '-r',
                            (f"rest:https://"
                             f"{backup_data[row].rest_user}:{backup_data[row].rest_password}"
                             f"@"
                             f"{backup_data[row].rest_domain}:{backup_data[row].rest_port}"
                             f"/"
                             f"{backup_data[row].rest_folder}"),
                            'stats']

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw

                # backup_data[row].rest, bool(backup_data[row].password)
                case ["1", True]:
                    # Restic function
                    args = [restic,
                            '-r',
                            (f"rest:https://"
                             f"{backup_data[row].rest_user}:{backup_data[row].rest_password}"
                             f"@"
                             f"{backup_data[row].rest_domain}:{backup_data[row].rest_port}"
                             f"/"
                             f"{backup_data[row].rest_folder}"),
                            'stats']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

                case _:
                    msg_box_error(self, "Pattern match not found")
                    return False

        except subprocess.CalledProcessError as error:
            # Process don't successful, send signal
            self.stats_error.emit(error.stderr)
        else:
            # Process successful, send signal
            self.stats_finished.emit(result.stdout)

        finally:
            pass


###############################################
# Worker for restic_backup
###############################################


class WorkerResticBackup(QObject):
    """ Worker Class for Rest function restic_backup """

    backup_finished = pyqtSignal(str)
    backup_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        """ Worker for restic_backup """

        restic = ResticDev.get(self)

        # We get the current row from the List widget
        row = ActiveRow.get(self)

        try:
            # REST == 0
            if backup_data[row].rest == "0":

                if not backup_data[row].password:
                    # Restic function
                    if backup_data[row].exclude == "":
                        args = [restic,
                                '-r',
                                backup_data[row].repository,
                                'backup',
                                backup_data[row].source]

                        result = subprocess.run(args,
                                                input=pass_word.pw[0],
                                                check=True,
                                                capture_output=True,
                                                text=True)

                        # Delete the variable from the input
                        del pass_word.pw
                    else:
                        args = [restic,
                                '-r',
                                backup_data[row].repository,
                                'backup',
                                backup_data[row].source,
                                '--exclude-file=%s' % (backup_data[row].exclude)]

                        result = subprocess.run(args,
                                                input=pass_word.pw[0],
                                                check=True,
                                                capture_output=True,
                                                text=True)

                        # Delete the variable from the input
                        del pass_word.pw
                else:
                    if backup_data[row].exclude == "":
                        args = [restic,
                                '-r',
                                backup_data[row].repository,
                                'backup',
                                backup_data[row].source]

                        result = subprocess.run(args,
                                                input=backup_data[row].password,
                                                check=True,
                                                capture_output=True,
                                                text=True)

                    else:
                        args = [restic,
                                '-r',
                                backup_data[row].repository,
                                'backup',
                                backup_data[row].source,
                                '--exclude-file=%s' % (backup_data[row].exclude)]

                        result = subprocess.run(args,
                                                input=backup_data[row].password,
                                                check=True,
                                                capture_output=True,
                                                text=True)

            else:
                # BACKUP for REST == 1
                if not backup_data[row].password:

                    if backup_data[row].exclude == "":
                        # Restic function
                        args = [restic,
                                '-r',
                                'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                                 backup_data[row].rest_password,
                                                                 backup_data[row].rest_domain,
                                                                 backup_data[row].rest_port,
                                                                 backup_data[row].rest_folder),
                                'backup',
                                backup_data[row].source]

                        result = subprocess.run(args,
                                                input=pass_word.pw[0],
                                                check=True,
                                                capture_output=True,
                                                text=True)

                        # Delete the variable from the input
                        del pass_word.pw
                    else:
                        # Restic function
                        args = [restic,
                                '-r',
                                'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                                 backup_data[row].rest_password,
                                                                 backup_data[row].rest_domain,
                                                                 backup_data[row].rest_port,
                                                                 backup_data[row].rest_folder),
                                'backup',
                                backup_data[row].source,
                                '--exclude-file=%s' % (backup_data[row].exclude)]

                        result = subprocess.run(args,
                                                input=pass_word.pw[0],
                                                check=True,
                                                capture_output=True,
                                                text=True)

                        # Delete the variable from the input
                        del pass_word.pw

                else:
                    if backup_data[row].exclude == "":
                        # Restic function
                        args = [restic,
                                '-r',
                                'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                                 backup_data[row].rest_password,
                                                                 backup_data[row].rest_domain,
                                                                 backup_data[row].rest_port,
                                                                 backup_data[row].rest_folder),
                                'backup',
                                backup_data[row].source]

                        result = subprocess.run(args,
                                                input=backup_data[row].password,
                                                check=True,
                                                capture_output=True,
                                                text=True)

                    else:
                        # Restic function
                        args = [restic,
                                '-r',
                                'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                                 backup_data[row].rest_password,
                                                                 backup_data[row].rest_domain,
                                                                 backup_data[row].rest_port,
                                                                 backup_data[row].rest_folder),
                                'backup',
                                backup_data[row].source,
                                '--exclude-file=%s' % (backup_data[row].exclude)]

                        result = subprocess.run(args,
                                                input=backup_data[row].password,
                                                check=True,
                                                capture_output=True,
                                                text=True)

        except subprocess.CalledProcessError as error:
            # Process don't successful, send signal
            self.backup_error.emit(error.stderr)
            ini.logging.error(f" {backup_data[row].name}: Backup job failed")
        else:
            # Process successful, send signal
            self.backup_finished.emit(result.stdout)
            ini.logging.info(f" {backup_data[row].name}: Backup job done")
            push("Restic UI", f" {backup_data[row].name}: Backup job done")

        finally:
            pass


###############################################
# Worker for restic_snapshots
###############################################


class WorkerResticSnapshots(QObject):
    """ Worker Class for Rest function restic_snapshots """

    snapshots_finished = pyqtSignal(str)
    snapshots_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        """ Worker for restic_backup """

        restic = ResticDev.get(self)

        # We get the current row from the List widget
        row = ActiveRow.get(self)
        # Processing the subprocess.run
        try:
            # REST == 0
            if backup_data[row].rest == "0":

                if not backup_data[row].password:
                    # Restic function
                    args = [restic,
                            '-r',
                            backup_data[row].repository,
                            'snapshots']

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw

                else:
                    # Restic function
                    args = [restic,
                            '-r',
                            backup_data[row].repository,
                            'snapshots']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

            else:

                if not backup_data[row].password:
                    # Restic function
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'snapshots']

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:
                    # Restic function

                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'snapshots']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

        except subprocess.CalledProcessError as error:
            # Process don't successful, send signal
            self.snapshots_error.emit(error.stderr)
        else:
            # Process successful, send signal
            SnapshotID.id_append(self, result.stdout)

            self.snapshots_finished.emit(result.stdout)

        finally:
            pass


###############################################
# Worker for restic_ls2
###############################################


class WorkerResticListSnapshot(QObject):
    """ Worker Class for Rest function restic_ls2 """

    ls2_finished = pyqtSignal(str)
    ls2_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        """Worker for restic_ls2"""

        restic = ResticDev.get(self)

        # We get the current row from the List widget
        row = ActiveRow.get(self)
        # Processing the subprocess.run
        try:

            if backup_data[row].rest == "0":
                if not backup_data[row].password:
                    # Restic function
                    args = [restic,
                            '-r',
                            backup_data[row].repository,
                            'ls',
                            snap_id.id1]

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:
                    # Restic function
                    args = [restic,
                            '-r',
                            backup_data[row].repository,
                            'ls',
                            snap_id.id1]

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

            else:
                if not backup_data[row].password:
                    # Restic function
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'ls',
                            snap_id.id1]

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:
                    # Restic function
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'ls',
                            snap_id.id1]

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

        except subprocess.CalledProcessError as error:
            # Process don't successful, send signal
            self.ls2_error.emit(error.stderr)
        else:
            self.ls2_finished.emit(result.stdout)

        finally:
            pass


###############################################
# Worker for init
###############################################


class WorkerResticInit(QObject):
    """ Worker Class for Rest function init """

    init_finished = pyqtSignal(str)
    init_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        """Worker for restic_init"""

        restic = ResticDev.get(self)

        # We get the current row from the List widget
        row = ActiveRow.get(self)

        # Processing the subprocess.run
        try:
            if backup_data[row].rest == "0":
                # INIT for normal backup
                if not backup_data[row].password:
                    # Restic function
                    if ini.settings.value('Restic_Test_Version') == "1":
                        args = [restic,
                                '-r',
                                backup_data[row].repository,
                                'init',
                                '--repository-version',
                                '2']
                    else:
                        args = [restic,
                                '-r',
                                backup_data[row].repository,
                                'init']

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:
                    # Restic function
                    if ini.settings.value('Restic_Test_Version') == "1":
                        args = [restic,
                                '-r',
                                backup_data[row].repository,
                                'init',
                                '--repository-version',
                                '2']
                    else:
                        args = [restic,
                                '-r',
                                backup_data[row].repository,
                                'init']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

            else:
                # INIT for REST backup
                if not backup_data[row].password:
                    # Restic function
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'init']

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:
                    # Restic function
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'init']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

        except subprocess.CalledProcessError as error:
            # Process don't successful, send signal
            self.init_error.emit(error.stderr)
            ini.logging.error(f" {backup_data[row].name}: Init job failed")
        else:
            # Normal operation if no error
            # Serves to write init status.

            # When creating a new backup using ADD and an INIT afterwards, the following occurred
            # IndexError: list index out of range
            # After creating a new backup, the key_list is not correct.
            # We're rebuilding these!

            keys_list = []

            # create new keys_list
            for key in backups:
                keys_list.append(key)

            if not backup_data[row].password:
                backups[keys_list[row]] = {'name': backup_data[row].name,
                                           'repository': backup_data[row].repository,
                                           'source': backup_data[row].source,
                                           'password': "",
                                           'init': "1",
                                           'exclude': backup_data[row].exclude,
                                           'rest': backup_data[row].rest,
                                           'rest_domain': backup_data[row].rest_domain,
                                           'rest_port': backup_data[row].rest_port,
                                           'rest_user': backup_data[row].rest_user,
                                           'rest_password': backup_data[row].rest_password,
                                           'rest_folder': backup_data[row].rest_folder}
            else:
                backups[keys_list[row]] = {'name': backup_data[row].name,
                                           'repository': backup_data[row].repository,
                                           'source': backup_data[row].source,
                                           'password': backup_data[row].password,
                                           'init': "1",
                                           'exclude': backup_data[row].exclude,
                                           'rest': backup_data[row].rest,
                                           'rest_domain': backup_data[row].rest_domain,
                                           'rest_port': backup_data[row].rest_port,
                                           'rest_user': backup_data[row].rest_user,
                                           'rest_password': backup_data[row].rest_password,
                                           'rest_folder': backup_data[row].rest_folder}

            # Write JSON
            with open(Path(ini.JSON_FILE), 'w', encoding='utf-8') as f:
                json.dump(backups, f)

            self.init_finished.emit(result.stdout)
            ini.logging.info(f" {backup_data[row].name}: Init job done")
            push("Restic UI", f" {backup_data[row].name}: Init job done")

        finally:
            pass


###############################################
# Worker for prune
###############################################


class WorkerResticPrune(QObject):
    """ Worker Class for Rest function restic_prune """

    prune_finished = pyqtSignal(str)
    prune_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        """Worker for restic_prune"""

        restic = ResticDev.get(self)

        # We get the current row from the List widget
        row = ActiveRow.get(self)

        keep_last = ini.settings.value("Restic_Keep_Last")
        keep_monthly = ini.settings.value("Restic_Keep_Monthly")

        # Processing the subprocess.run
        try:
            # restic function prune
            if backup_data[row].rest == "0":
                if not backup_data[row].password:
                    args = [restic,
                            '-r',
                            backup_data[row].repository,
                            'forget',
                            '--keep-last',
                            keep_last,
                            '--keep-monthly',
                            keep_monthly,
                            '--prune']

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:
                    args = [restic,
                            '-r',
                            backup_data[row].repository,
                            'forget',
                            '--keep-last',
                            keep_last,
                            '--keep-monthly',
                            keep_monthly,
                            '--prune']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

            else:
                if not backup_data[row].password:
                    # Restic function
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'forget',
                            '--keep-last',
                            keep_last,
                            '--keep-monthly',
                            keep_monthly,
                            '--prune']

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:
                    # Restic function
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'forget',
                            '--keep-last',
                            keep_last,
                            '--keep-monthly',
                            keep_monthly,
                            '--prune']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

        except subprocess.CalledProcessError as error:
            # Process don't successful, send signal
            self.prune_error.emit(error.stderr)
            ini.logging.error(f" {backup_data[row].name}: Prune job failed")

        else:
            # Normal operation if no error
            self.prune_finished.emit(result.stdout)
            ini.logging.info(f" {backup_data[row].name}: Prune job done")
            push("Restic UI", f" {backup_data[row].name}: Prune job done")

        finally:
            pass


###############################################
# Worker for check
###############################################


class WorkerResticCheck(QObject):
    """ Worker Class for Rest function restic_check """

    check_finished = pyqtSignal(str)
    check_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        """Worker for restic_check"""

        check = ini.settings.value("Restic_Check_Percent")
        check2 = f"{check}%"

        restic = ResticDev.get(self)

        # We get the current row from the List widget
        row = ActiveRow.get(self)

        # Processing the subprocess.run
        result = None
        try:
            match_list = [backup_data[row].rest, bool(backup_data[row].password)]

            match match_list:

                # backup_data[row].rest, bool(backup_data[row].password)
                case ["0", False]:
                # restic function check
                    args = [restic,
                            '-r',
                            backup_data[row].repository,
                            'check',
                            '--read-data-subset',
                            check2]

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw

                case["0", True]:
                    # restic function check
                    args = [restic,
                            '-r',
                            backup_data[row].repository,
                            'check',
                            '--read-data-subset',
                            check2]

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

                case["1", False]:
                    # restic function check
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'check',
                            '--read-data-subset',
                            check2]

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw

                case["1", True]:
                    # restic function check
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'check',
                            '--read-data-subset',
                            check2]

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

                case _:
                    msg_box_error(self, "Pattern match not found")
                    return False

        except subprocess.CalledProcessError as error:
            # Process don't successful, send signal
            self.check_error.emit(error.stderr)

        else:
            # Normal operation if no error
            self.check_finished.emit(result.stdout)

        finally:
            pass


###############################################
# Worker for unlock
###############################################


class WorkerResticUnlock(QObject):
    """ Worker Class for Rest function check """

    unlock_finished = pyqtSignal(str)
    unlock_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        """Worker for restic_unlock"""

        restic = ResticDev.get(self)

        # We get the current row from the List widget
        row = ActiveRow.get(self)
        # Processing the subprocess.run
        try:
            # restic function check
            if backup_data[row].rest == "0":
                if not backup_data[row].password:
                    # Restic function
                    args = [restic,
                            '-r',
                            backup_data[row].repository,
                            'unlock']

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:
                    # Restic function
                    args = [restic,
                            '-r',
                            backup_data[row].repository,
                            'unlock']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

            else:
                if not backup_data[row].password:
                    # Restic function
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'unlock']

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:
                    # Restic function
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'unlock']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

        except subprocess.CalledProcessError as error:
            # Process don't successful, send signal
            self.unlock_error.emit(error.stderr)

        else:
            # Normal operation if no error
            self.unlock_finished.emit(result.stdout)

        finally:
            pass


###############################################
# Worker for restore
###############################################


class WorkerResticRestore(QObject):
    """ Worker Class for Rest function restic_restore """

    restore_finished = pyqtSignal(str)
    restore_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        """Worker for restic_restore"""

        restic = ResticDev.get(self)

        # We get the current row from the List widget
        row = ActiveRow.get(self)
        # Processing the subprocess.run
        try:
            # restic function restore
            if backup_data[row].rest == "0":
                if not backup_data[row].password:
                    args = [restic, '-r',
                            backup_data[row].repository,
                            'restore',
                            snap_id.id1,
                            '--target',
                            res_path.restore]

                    subprocess.run(args,
                                   input=pass_word.pw[0],
                                   check=True,
                                   capture_output=True,
                                   text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:
                    args = [restic, '-r',
                            backup_data[row].repository,
                            'restore',
                            snap_id.id1,
                            '--target',
                            res_path.restore]

                    subprocess.run(args,
                                   input=backup_data[row].password,
                                   check=True,
                                   capture_output=True,
                                   text=True)

            else:
                if not backup_data[row].password:
                    # Restic function
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'restore',
                            snap_id.id1,
                            '--target',
                            res_path.restore]

                    subprocess.run(args,
                                   input=pass_word.pw[0],
                                   check=True,
                                   capture_output=True,
                                   text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:
                    # Restic function
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'restore',
                            snap_id.id1,
                            '--target',
                            res_path.restore]

                    subprocess.run(args,
                                   input=backup_data[row].password,
                                   check=True,
                                   capture_output=True,
                                   text=True)

        except subprocess.CalledProcessError as error:
            # Process don't successful, send signal
            self.restore_error.emit(error.stderr)

        else:
            # Normal operation if no error
            try:
                process = subprocess.run(['ls',
                                          '-lha',
                                          res_path.restore],
                                         stdout=subprocess.PIPE,
                                         universal_newlines=True)

                output = process.stdout
            except subprocess.CalledProcessError as error:
                self.mount_error.emit(error)

            else:
                self.restore_finished.emit(output)

        finally:
            pass


###############################################
# Worker for mount
###############################################


class WorkerResticMount(QObject):
    """ Worker Class for Rest function restic_mount """

    mount_finished = pyqtSignal(str)
    mount_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        """ Worker for restic_mount """

        restic = ResticDev.get(self)

        # We get the current row from the List widget
        row = ActiveRow.get(self)

        # Processing the subprocess.run
        process = None
        try:
            if backup_data[row].rest == "0":
                # if no password is in backup_data
                if not backup_data[row].password:

                    # Create cmd
                    cmd = [restic,
                           '-r',
                           backup_data[row].repository,
                           'mount',
                           m_path[0].path]

                    # We create the object, here with stdin and open the asynchronous subprocess
                    process = subprocess.Popen(
                        cmd, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
                    process.stdin.write(pass_word.pw[0].encode())
                    process.stdin.close()

                    # Delete the variable from the input
                    del pass_word.pw

                # if password is in backup_data
                else:
                    # Create cmd
                    cmd = [restic,
                           '-r',
                           backup_data[row].repository,
                           'mount',
                           m_path[0].path]

                    pwd = backup_data[row].password.encode()

                    # We create the object, here with stdin and open the asynchronous subprocess
                    process = subprocess.Popen(
                        cmd, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
                    process.stdin.write(pwd)
                    process.stdin.close()
            else:
                # if no password is in backup_data
                if not backup_data[row].password:

                    # Create cmd
                    cmd = [restic,
                           '-r',
                           'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                            backup_data[row].rest_password,
                                                            backup_data[row].rest_domain,
                                                            backup_data[row].rest_port,
                                                            backup_data[row].rest_folder),
                           'mount',
                           m_path[0].path]

                    # We create the object, here with stdin and open the asynchronous subprocess
                    process = subprocess.Popen(
                        cmd, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
                    process.stdin.write(pass_word.pw[0].encode())
                    process.stdin.close()

                    # Delete the variable from the input
                    del pass_word.pw

                # if password is in backup_data
                else:
                    # Create cmd
                    cmd = [restic,
                           '-r',
                           'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                            backup_data[row].rest_password,
                                                            backup_data[row].rest_domain,
                                                            backup_data[row].rest_port,
                                                            backup_data[row].rest_folder),
                           'mount',
                           m_path[0].path]

                    pwd = backup_data[row].password.encode()

                    # We create the object, here with stdin and open the asynchronous subprocess
                    process = subprocess.Popen(
                        cmd, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
                    process.stdin.write(pwd)
                    process.stdin.close()

        except subprocess.CalledProcessError as error:
            pass

        finally:
            # we give some time for our subprocess.Popen
            time.sleep(1.0)

            try:
                if process.poll() == 1:
                    raise Exception(
                        """The Restic mount command was not successful.
                        Probable cause: wrong password or no key found""")

                else:
                    # Mount process successful
                    # We fetch the directory content
                    try:
                        process = subprocess.run(['ls',
                                                  '-lha',
                                                  m_path[0].path],
                                                 stdout=subprocess.PIPE,
                                                 universal_newlines=True)

                        output = process.stdout

                    except subprocess.CalledProcessError as error:
                        self.mount_error.emit("Error while fetching the directory contents")

                    else:
                        self.mount_finished.emit(output)

            except Exception as Error:
                self.mount_error.emit(repr(Error))


###############################################
# Worker for clear cache
###############################################


class WorkerResticClearCache(QObject):
    """ Worker Class for Rest function clear cache """

    cache_finished = pyqtSignal(str)
    cache_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        """ Worker for restic cache --cleanup """

        # We get the current row from the List widget
        row = ActiveRow.get(self)
        # Processing the subprocess.run
        try:
            # restic function check
            if backup_data[row].rest == "0":
                if not backup_data[row].password:
                    # Restic function
                    args = ['restic',
                            '-r',
                            backup_data[row].repository,
                            'cache',
                            '--cleanup']

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:
                    # Restic function
                    args = ['restic',
                            '-r',
                            backup_data[row].repository,
                            'cache',
                            '--cleanup']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

            else:
                if not backup_data[row].password:
                    # Restic function
                    args = ['restic',
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'cache',
                            '--cleanup']

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:
                    # Restic function
                    args = ['restic',
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'cache',
                            '--cleanup']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

        except subprocess.CalledProcessError as error:

            # Process don't successful, send signal
            self.cache_error.emit(error.stderr)

        else:
            # Normal operation if no error
            self.cache_finished.emit(result.stdout)

        finally:
            pass


###############################################
# Worker for migrate check
###############################################


class WorkerResticCheckMigrate(QObject):
    """ Worker Class for Rest function migrate check """

    migrate_check_finished = pyqtSignal(str)
    migrate_check_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        """ Worker for restic migrate -r REPO_PATH """

        restic = ResticDev.get(self)

        # We get the current row from the List widget
        row = ActiveRow.get(self)
        # Processing the subprocess.run
        try:
            # restic function migrate check
            if backup_data[row].rest == "0":
                if not backup_data[row].password:

                    args = [restic,
                            'migrate',
                            '-r',
                            backup_data[row].repository]

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:

                    args = [restic,
                            'migrate',
                            '-r',
                            backup_data[row].repository]

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

            else:
                if not backup_data[row].password:

                    args = [restic,
                            'migrate',
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder)]

                    result = subprocess.run(args,
                                            input=pass_word.pw[0],
                                            check=True,
                                            capture_output=True,
                                            text=True)

                    # Delete the variable from the input
                    del pass_word.pw
                else:

                    args = [restic,
                            'migrate',
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder)]

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

        except subprocess.CalledProcessError as error:
            # Process don't successful, send signal
            self.migrate_check_error.emit(error.stderr)

        else:
            # Normal operation if no error
            self.migrate_check_finished.emit(result.stdout)

        finally:
            pass


###############################################
# Worker for migrate update
###############################################


class WorkerResticMigrateUpdate(QObject):
    """ Worker Class for Rest function migrate update """

    migrate_update_finished = pyqtSignal(str)
    migrate_update_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        """ Worker for restic migrate upgrade_repo_v2 -r REPO_PATH """

        restic = ResticDev.get(self)

        # We get the current row from the List widget
        row = ActiveRow.get(self)

        # Processing the subprocess.run
        try:

            if backup_data[row].rest == "0":
                if not backup_data[row].password:

                    args = [restic,
                            'migrate',
                            'upgrade_repo_v2',
                            '-r',
                            backup_data[row].repository]

                    result = subprocess.run(args,
                                            env=dict(os.environ, RESTIC_PASSWORD=backup_data[row].password),
                                            check=True,
                                            capture_output=True,
                                            text=True)

                else:

                    args = [restic,
                            'migrate',
                            'upgrade_repo_v2',
                            '-r',
                            backup_data[row].repository]

                    result = subprocess.run(args,
                                            env=dict(os.environ, RESTIC_PASSWORD=backup_data[row].password),
                                            check=True,
                                            capture_output=True,
                                            text=True)

            else:
                if not backup_data[row].password:

                    args = [restic,
                            'migrate',
                            'upgrade_repo_v2',
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder)]

                    result = subprocess.run(args,
                                            env=dict(os.environ, RESTIC_PASSWORD=backup_data[row].password),
                                            check=True,
                                            capture_output=True,
                                            text=True)

                else:

                    args = [restic,
                            'migrate',
                            'upgrade_repo_v2',
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder)]

                    result = subprocess.run(args,
                                            env=dict(os.environ, RESTIC_PASSWORD=backup_data[row].password),
                                            check=True,
                                            capture_output=True,
                                            text=True)

        except subprocess.CalledProcessError as error:
            # Process don't successful, send signal
            print("TEST")
            self.migrate_update_error.emit(error.stderr)

        else:
            # Normal operation if no error
            self.migrate_update_finished.emit(result.stdout)

        finally:
            pass


###############################################
# Worker for call_repo_version
###############################################


class WorkerResticCallRepoVersion(QObject):
    """ Worker Class for call repo version, v1 or v2? """

    call_repo_version_finished = pyqtSignal(str)
    call_repo_version_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()

    def run(self):
        restic = ResticDev.get(self)

        # We get the current row from the List widget
        row = ActiveRow.get(self)

        result = None
        try:
            # restic function check
            match_list = [backup_data[row].rest, bool(backup_data[row].password)]
            match match_list:

                # backup_data[row].rest, bool(backup_data[row].password)
                case ["0", False]:
                    # Create an * @ position i[54]
                    i = '*' * 55
                    self.call_repo_version_finished.emit(i)
                    return False

                # backup_data[row].rest, bool(backup_data[row].password)
                case ["0", True]:
                    # check data
                    args = [restic,
                            '-r',
                            backup_data[row].repository,
                            'cat',
                            'config']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)
                    print(result.stderr)

                # backup_data[row].rest, bool(backup_data[row].password)
                case ["1", False]:
                    # Create an * @ position i[54]
                    i = '*' * 55
                    self.call_repo_version_finished.emit(i)
                    return False

                # backup_data[row].rest, bool(backup_data[row].password)
                case ["1", True]:
                    # Restic function
                    args = [restic,
                            '-r',
                            'rest:https://%s:%s@%s:%s/%s' % (backup_data[row].rest_user,
                                                             backup_data[row].rest_password,
                                                             backup_data[row].rest_domain,
                                                             backup_data[row].rest_port,
                                                             backup_data[row].rest_folder),
                            'cat',
                            'config']

                    result = subprocess.run(args,
                                            input=backup_data[row].password,
                                            check=True,
                                            capture_output=True,
                                            text=True)

                case _:
                    msg_box_error(self, "Pattern match not found")
                    return False

        except subprocess.CalledProcessError as error:
            # Process don't successful, send signal
            self.call_repo_version_error.emit(error.stderr)

        else:
            # Normal operation if no error
            self.call_repo_version_finished.emit(result.stdout)

        finally:
            pass


#####################
# Save Dev Version
#####################


class ResticDev():
    """ Store dev version yes or no """

    #####################
    # Determine restic version!
    #####################
    def get(self):
        """
        """
        if ini.settings.value('Restic_Test_Version') == "1":
            # We get the path to the file from the settings
            p = PurePath(ini.settings.value('Restic_DEV'))
            restic = f'{p.parents[0]}/./{p.name}'

            # check if restic dev version is executable
            try:
                args = [restic, 'version']

                subprocess.run(args,
                               check=True,
                               capture_output=True,
                               text=True)

            except OSError:
                print(
                    "Error: The Restic Dev version is not executable. Please change! Switched to the normal Restic version.")
                push("Restic UI",
                     "Error: The Restic Dev version is not executable. Please change! Switched to the normal Restic version.")
                mainWin.restic_switch()
                restic = 'restic'

        else:
            restic = 'restic'
        return restic


#####################
# Save Snapshot IDs
#####################

class SnapshotID:
    """
    Structure of snapshot_id
    [4, 'f00c870b', '5de67304']
    4 = actual row (row)
    f00c870b = ID Repository
    5de67304 = ID Repository
    ID Repositories are variable!
    """

    def __init__(self, id1):
        """ Snapshot ID from input in restic_ls2 """
        self.id1 = id1

    def id_remove(self):
        """ If the list snapshot_id is not exactly 1, then delete! """

        if not snapshot_id == ['']:

            for count, value in enumerate(snapshot_id):
                snapshot_id.remove(snapshot_id[count])

    def id_append(self, result):

        """
        What do we do here? We get the ID's of the repositories
        and save them in a list.

        Why?
        I want to store the ID's in a list to be able to query them
        with the List Snapshot function to see if they exist.

        Save current row in snapshot_id[0].
        snapshot_id[0] = mainWin.listWidget.currentRow() # removed for testing!!

        We split the result into individual lines.
        Result is passed and a list is generated
        """

        result_list = result.split('\n')

        # We loop through the list and output the ID's and store in snapshot_id
        for count, value in enumerate(result_list):
            # Save data only if it comes from a row with snapshot ID.
            # But this causes problems if you have snapshots with different source entries!?!?
            # if backup_data[mainWin.listWidget.currentRow()].source in value:
            snapshot_id.append(value[0:8])


# We create the list, with an empty entry [0].


snapshot_id = []
snapshot_id.append("")

snap_id = SnapshotID("0")


#####################
# Save Snapshot IDs
#####################


class RestorePath():
    """ Class to store restore path from input field """

    def __init__(self, restore):
        """ Snapshot ID from input in restic_ls2 """
        self.restore = restore


res_path = RestorePath("0")


#####################
# Call ActiveRow()
#####################


class ActiveRow():

    #####################
    # Determine selected backup!
    #####################
    def get(self):
        """
        """
        active_row = mainWin.listWidget.currentRow()
        return active_row

    #####################
    # Reloads the backup into the widget
    #####################

    def click(self, row):
        """
        """

        # we start the worker thread
        mainWin.thread_call_repo_version.start()

        # we start waitingspinnerwidget
        spinner.start()


#####################
# Set pwd
#####################


class Password():
    """ Class to store password for the job """

    def __init__(self, pw):
        self.pw = pw


# We set the value to 0
pass_word = Password(0)


#####################
# Status Snapshots Cancel
#####################


class StatusSnapshots():
    """ Class to store status from Snapshots """

    def __init__(self, status_snapshots):
        self.status_snapshots = status_snapshots


# We set the value to 0
status = StatusSnapshots(0)


#####################
# MountPath()
#####################


class MountPath():
    """ Class to store Mount Path for funktion mount & umount """

    def __init__(self, path):
        self.path = path


# We create the list, with an empty entry [0].
m_path = []
m_path.append(MountPath(""))


#####################
# Call BackupList()
#####################


class BackupList():
    """ I build here a class to manage the backup list
    Data is in backup_list.json """

    #####################
    # Init
    #####################

    def __init__(self,
                 name,
                 repository,
                 source,
                 password,
                 init,
                 exclude,
                 rest,
                 rest_domain,
                 rest_port,
                 rest_user,
                 rest_password,
                 rest_folder):

        self.name = name
        self.repository = repository
        self.source = source
        self.password = password
        self.init = init
        self.exclude = exclude
        self.rest = rest
        self.rest_domain = rest_domain
        self.rest_port = rest_port
        self.rest_user = rest_user
        self.rest_password = rest_password
        self.rest_folder = rest_folder

    #####################
    # Save JSON
    #####################

    def save_json(self):
        """ We store the JSON file for data backup. """

        try:
            with open(Path(ini.JSON_FILE), 'w', encoding='utf-8') as f:
                json.dump(backups, f)
                msg_box(self, "File saved")
        except OSError as error:
            msg_box_error(self, error)

    #####################
    # Load JSON
    #####################

    def load_json(self):
        """
        """
        # Load data

        # Clear widget
        mainWin.listWidget.clear()

        # Delete key_list
        keys_list = []

        # redo keys_list
        for key in backups:
            keys_list.append(key)

        # Important, we must also create the new objects!
        backup_data = []
        for count, value in enumerate(keys_list):
            backup_data.append(BackupList(backups[keys_list[count]]['name'],
                                          backups[keys_list[count]]['repository'],
                                          backups[keys_list[count]]['source'],
                                          backups[keys_list[count]]['password'],
                                          backups[keys_list[count]]['init'],
                                          backups[keys_list[count]]['exclude'],
                                          backups[keys_list[count]]['rest'],
                                          backups[keys_list[count]]['rest_domain'],
                                          backups[keys_list[count]]['rest_port'],
                                          backups[keys_list[count]]['rest_user'],
                                          backups[keys_list[count]]['rest_password'],
                                          backups[keys_list[count]]['rest_folder']))

        for count, value in enumerate(keys_list):
            mainWin.listWidget.addItem(backup_data[count].name)

        ActiveRow.click(self, 0)

        mainWin.listWidget.setCurrentRow(0)

    #####################
    # Delete Entry
    #####################

    def del_entry(self, row):
        """
        """
        # We delete a single entry from the list
        # Delete key_list
        keys_list = []

        # Important, we must also create the new objects!
        for count, value in enumerate(keys_list):
            backup_data.append(BackupList(backups[keys_list[count]]['name'],
                                          backups[keys_list[count]]['repository'],
                                          backups[keys_list[count]]['source'],
                                          backups[keys_list[count]]['password'],
                                          backups[keys_list[count]]['init'],
                                          backups[keys_list[count]]['exclude'],
                                          backups[keys_list[count]]['rest'],
                                          backups[keys_list[count]]['rest_domain'],
                                          backups[keys_list[count]]['rest_port'],
                                          backups[keys_list[count]]['rest_user'],
                                          backups[keys_list[count]]['rest_password'],
                                          backups[keys_list[count]]['rest_folder']))

        for key in backups:
            keys_list.append(key)

        backups.pop(keys_list[row])

        with open(Path(ini.JSON_FILE), 'w', encoding='utf-8') as f:
            json.dump(backups, f)

        BackupList.objects_edit(self)

        row = None

        # We redo the classes objects
        BackupList.make_key_list_new(self)

        mainWin.widget.setPlainText("")
        mainWin.statusBar().showMessage("")
        mainWin.listWidget.clear()
        BackupList.load_json(self)

    #####################
    # Edit object
    #####################

    def objects_edit(self):
        """
        """
        # Delete key_list
        keys_list = []

        # redo keys_list
        for key in backups:
            keys_list.append(key)

        # Important, we must also create the new objects!
        for count, value in enumerate(keys_list):
            backup_data[count] = BackupList(backups[keys_list[count]]['name'],
                                            backups[keys_list[count]]['repository'],
                                            backups[keys_list[count]]['source'],
                                            backups[keys_list[count]]['password'],
                                            backups[keys_list[count]]['init'],
                                            backups[keys_list[count]]['exclude'],
                                            backups[keys_list[count]]['rest'],
                                            backups[keys_list[count]]['rest_domain'],
                                            backups[keys_list[count]]['rest_port'],
                                            backups[keys_list[count]]['rest_user'],
                                            backups[keys_list[count]]['rest_password'],
                                            backups[keys_list[count]]['rest_folder'])

        mainWin.listWidget.clear()

        # We get the names from the list
        for count, value in enumerate(keys_list):
            mainWin.listWidget.addItem(backup_data[count].name)

    #####################
    # Make objects new
    #####################

    def objects_new(self):
        """
        """
        # Delete key_list
        keys_list = []

        # redo keys_list
        for key in backups:
            keys_list.append(key)

        # Important, we must also create the new objects!
        x = int(list(backups)[-1])

        backup_data.append(BackupList(backups[x]['name'],
                                      backups[x]['repository'],
                                      backups[x]['source'],
                                      backups[x]['password'],
                                      backups[x]['init'],
                                      backups[x]['exclude'],
                                      backups[x]['rest'],
                                      backups[x]['rest_domain'],
                                      backups[x]['rest_port'],
                                      backups[x]['rest_user'],
                                      backups[x]['rest_password'],
                                      backups[x]['rest_folder']))

        mainWin.listWidget.clear()

        # We get the names from the list
        for count, value in enumerate(backup_data):
            mainWin.listWidget.addItem(backup_data[count].name)

    #####################
    # Make key_list new
    #####################

    def make_key_list_new(self):
        """
        """
        keys_list = []

        # redo keys_list
        for key in backups:
            keys_list.append(key)


###############################################
# Init the keys_list and the backup_data list.
###############################################
"When creating the class, we initialize it with the values from the file 'backup_list.json' "

# File 'backup_list.enc' will be decrypted before we can use it!
if Path(ini.CRYPT_FILE).exists():
    ini.settings.setValue("Crypt", 2)
    # ini.settings.setValue("Key", 1)

    # Load key
    key = Crypt.load_key()

    if key == -1:
        raise Exception("Key File Error. Fix it in 'Restic_UI.conf!")

    path = f"{ini.USERHOME}/.restic_ui/backup_list.enc"

    Crypt.decrypt(path, key)

    key = ""
else:
    pass

# No file 'backup_list.json' or 'backup_list.enc' in .restic_ui
if not Path(ini.JSON_FILE).exists():
    ini.settings.setValue("Crypt", 0)
    ini.settings.setValue("Key_Path", "")
    import create

    try:
        with open(Path(ini.JSON_FILE), 'r', encoding='utf-8') as f:
            # with open('backup_list.json', 'r', encoding='utf-8') as f:
            backups = json.load(f)

            # We get the number of entries in the object backups
            # and put them in the keys_list! ['0', '1', '2', 3]
            keys_list = []
            for key in backups:
                keys_list.append(key)

            # We create the objects backup_data and fill them with data
            backup_data = []
            for count, value in enumerate(keys_list):
                # After that we create an entry under backup_data.append etc.
                backup_data.append(BackupList(backups[keys_list[count]]['name'],
                                              backups[keys_list[count]]['repository'],
                                              backups[keys_list[count]]['source'],
                                              backups[keys_list[count]]['password'],
                                              backups[keys_list[count]]['init'],
                                              backups[keys_list[count]]['exclude'],
                                              backups[keys_list[count]]['rest'],
                                              backups[keys_list[count]]['rest_domain'],
                                              backups[keys_list[count]]['rest_port'],
                                              backups[keys_list[count]]['rest_user'],
                                              backups[keys_list[count]]['rest_password'],
                                              backups[keys_list[count]]['rest_folder']))

    except IOError:
        print("File Error!")
else:
    # File 'backup_list.json' is in folder .restic_ui
    try:
        with open(Path(ini.JSON_FILE), 'r', encoding='utf-8') as f:
            # with open('backup_list.json', 'r', encoding='utf-8') as f:
            backups = json.load(f)

            # We get the number of entries in the object backups
            # and put them in the keys_list! ['0', '1', '2', 3]
            keys_list = []
            for key in backups:
                keys_list.append(key)

            # We create the objects backup_data and fill them with data
            backup_data = []
            for count, value in enumerate(keys_list):
                # After that we create an entry under backup_data.append etc.
                backup_data.append(BackupList(backups[keys_list[count]]['name'],
                                              backups[keys_list[count]]['repository'],
                                              backups[keys_list[count]]['source'],
                                              backups[keys_list[count]]['password'],
                                              backups[keys_list[count]]['init'],
                                              backups[keys_list[count]]['exclude'],
                                              backups[keys_list[count]]['rest'],
                                              backups[keys_list[count]]['rest_domain'],
                                              backups[keys_list[count]]['rest_port'],
                                              backups[keys_list[count]]['rest_user'],
                                              backups[keys_list[count]]['rest_password'],
                                              backups[keys_list[count]]['rest_folder']))

    except IOError:
        print("File Error!")


###############################################
# Second Window / Add Backup
###############################################


class AddBackupWindow(QWidget):
    """
    """

    def __init__(self):
        super().__init__()

        ###############################################
        # Add Backup
        ###############################################

        def add_backup():
            """
            """
            # check for loaded data

            row = ActiveRow.get(self)

            if row == -1:
                msg_box_error(self, "Please load data before add!")
                # clear line-edits
                clear_lineedit()
                return False

            else:
                read_lineedit()

                # What do we do here? We get the number of keys in backups
                # print(list(backups)[-1]) result is last key as string
                # We convert the string to INT and add +1!

                new_key = int(list(backups)[-1]) + 1

                # all three mandatory fields filled out?
                if self.in1 and self.in2 and self.in3:

                    backups[new_key] = {'name': self.in1,
                                        'repository': self.in2,
                                        'source': self.in3,
                                        'password': self.in4,
                                        'init': "0",
                                        'exclude': self.in5,
                                        'rest': "0",
                                        'rest_domain': "",
                                        'rest_port': "",
                                        'rest_user': "",
                                        'rest_password': "",
                                        'rest_folder': ""}
                else:
                    msg_box(self, "Please fill in all three fields!")

                    # clear line-edits
                    clear_lineedit()

                    mainWin.statusBar().showMessage("Please select a backup!")

                    return False

                # We save the entry to the JSON
                BackupList.save_json(self)

                # UI
                mainWin.widget.setPlainText("")

                mainWin.statusBar().showMessage("")

                # We redo the classes objects
                BackupList.objects_new(self)

                # clear line-edits
                clear_lineedit()

                # load JSON
                BackupList.load_json(self)

                last_element = len(keys_list) - 1

                ActiveRow.click(self, (last_element + 1))

                mainWin.listWidget.setCurrentRow((last_element + 1))

        ###############################################
        # Add REST-Backup
        ###############################################

        def add_rest_backup():
            """
            """
            row = ActiveRow.get(self)

            if row == -1:
                msg_box_error(self, "Please load data before add!")
                # clear line-edits
                clear_lineedit()
                return False

            else:
                read_lineedit()

                # What do we do here? We get the number of keys in backups
                # print(list(backups)[-1]) result is last key as string
                # We convert the string to INT and add +1!

                new_key = int(list(backups)[-1]) + 1

                # two mandatory fields filled out?
                if self.in1 and self.in3:

                    backups[new_key] = {'name': self.in1,
                                        'repository': "",
                                        'source': self.in3,
                                        'password': self.in4,
                                        'init': "0",
                                        'exclude': self.in5,
                                        'rest': "1",
                                        'rest_domain': self.in6,
                                        'rest_port': self.in7,
                                        'rest_user': self.in8,
                                        'rest_password': self.in9,
                                        'rest_folder': self.in10}
                else:
                    msg_box(self, "Please fill in all two fields!")

                    # clear line-edits
                    clear_lineedit()

                    mainWin.statusBar().showMessage("Please select a backup!")

                    return False

                # We save the entry to the JSON
                BackupList.save_json(self)

                # UI
                mainWin.widget.setPlainText("")

                mainWin.statusBar().showMessage("")

                # We redo the classes objects
                BackupList.objects_new(self)

                # clear line-edits
                clear_lineedit()

                # load JSON
                BackupList.load_json(self)

                last_element = len(keys_list) - 1

                ActiveRow.click(self, (last_element + 1))

                mainWin.listWidget.setCurrentRow((last_element + 1))

        ###############################################
        # get Repository Path
        ###############################################

        def get_repository_path():
            """
            """
            path = get_path(self, 'Home_Path', 'Repository Path')

            if not path:
                return False
            else:
                self.input_repository.setText(path)

        ###############################################
        # get Source_Path
        ###############################################

        def get_source_path():
            """
            """
            path = get_path(self, 'Source_Path', 'Source Path')

            if not path:
                return False
            else:
                self.input_source.setText(path)

        ###############################################
        # get_exclude_list
        ###############################################

        def get_exclude_list():
            """
            """
            path = get_file_path(self, 'Exclude_List', 'Exclude List')

            if not path[0]:
                return False
            else:
                self.input_exclude_list.setText(path[0])

        ###############################################
        # read line-edits
        ###############################################

        def read_lineedit():
            """ Here we read the objects of the line edits directly
            and store their content in a variable."""

            self.in1 = str(self.input_backup_name.text())
            self.in2 = str(self.input_repository.text())
            self.in3 = str(self.input_source.text())
            self.in4 = str(self.input_password.text())
            self.in5 = str(self.input_exclude_list.text())
            self.in6 = str(self.rest_domain.text())
            self.in7 = str(self.rest_port.text())
            self.in8 = str(self.rest_user.text())
            self.in9 = str(self.rest_password.text())
            self.in10 = str(self.rest_folder.text())

        ###############################################
        # clear line-edits
        ###############################################

        def clear_lineedit():
            """ Here we clear the line-edits when we call
            the add backup window for the second time."""

            self.input_backup_name.clear()
            self.input_repository.clear()
            self.input_source.clear()
            self.input_password.clear()
            self.input_exclude_list.clear()
            self.rest_domain.clear()
            self.rest_port.clear()
            self.rest_user.clear()
            self.rest_password.clear()
            self.rest_folder.clear()

            # set condition from checkbox_rest
            self.checkbox_rest.setChecked(False)

        ###############################################
        # Close
        ###############################################

        def close():
            """
            """
            clear_lineedit()
            self.close()

        ###############################################
        # checkbox-changed
        ###############################################

        def checkbox_changed(state):
            """Set state for input, button, etc."""

            if state:
                self.input_repository.hide()
                self.button_get_repository.hide()
                self.button2.hide()
                self.action_quit.hide()
                self.tabs.setTabEnabled(1, True)

            else:
                self.input_repository.show()
                self.button_get_repository.show()
                self.button2.show()
                self.action_quit.show()
                self.tabs.setTabEnabled(1, False)

        ###############################################
        # Window Definitions
        ###############################################

        self.title = 'Restic UI - Add Backup'
        self.setWindowTitle(self.title)
        self.setWindowModality(Qt.WindowModality.ApplicationModal)
        self.setWindowFlag(Qt.WindowType.WindowCloseButtonHint, False)
        self.setWindowFlag(Qt.WindowType.WindowMinimizeButtonHint, False)
        self.setMinimumSize(600, 400)
        self.setMaximumSize(600, 400)

        #####################
        # Button
        #####################

        # Add Backup
        self.button2 = QPushButton("", self)
        self.button2.setIcon(QIcon.fromTheme(
            'document-save', QIcon(ini.CWD + "/icons/document-save")))
        self.button2.clicked.connect(self.close)
        self.button2.clicked.connect(add_backup)

        # Get Repository Path
        self.button_get_repository = QPushButton("", self)
        self.button_get_repository.setIcon(QIcon.fromTheme(
            'document-open-folder', QIcon(ini.CWD + "/icons/folder-open")))
        self.button_get_repository.clicked.connect(get_repository_path)

        # Get Source Path
        self.button_get_source_path = QPushButton("", self)
        self.button_get_source_path.setIcon(QIcon.fromTheme(
            'document-open-folder', QIcon(ini.CWD + "/icons/folder-open")))
        self.button_get_source_path.clicked.connect(get_source_path)

        # Exclude List
        self.button_get_exclude_list = QPushButton("", self)
        self.button_get_exclude_list.setIcon(QIcon.fromTheme(
            'document-open-folder', QIcon(ini.CWD + "/icons/folder-open")))
        self.button_get_exclude_list.clicked.connect(get_exclude_list)

        # Quit Button Backup Tab
        self.action_quit = QPushButton("", self)
        self.action_quit.setIcon(QIcon.fromTheme(
            'application-exit', QIcon(ini.CWD + "/icons/exit")))
        self.action_quit.setToolTip("Quit without saving")
        self.action_quit.clicked.connect(close)

        # Save Backup
        self.button_save_tab_restic = QPushButton("", self)
        self.button_save_tab_restic.setIcon(QIcon.fromTheme(
            'document-save', QIcon(ini.CWD + "/icons/document-save")))
        self.button_save_tab_restic.clicked.connect(self.close)
        self.button_save_tab_restic.clicked.connect(add_rest_backup)

        # Quit Button REST Tab
        self.action_quit_2 = QPushButton("", self)
        self.action_quit_2.setIcon(QIcon.fromTheme(
            'application-exit', QIcon(ini.CWD + "/icons/exit")))
        self.action_quit_2.setToolTip("Quit without saving")
        self.action_quit_2.clicked.connect(close)

        #####################
        # Font etc.
        #####################

        # Create empty lines as spacers.
        self.label1_empty_line = QLabel()
        self.label2_empty_line = QLabel()
        self.label4_empty_line = QLabel()
        self.label1_optional = QLabel("* optional")

        self.label5_empty_line = QLabel()
        self.label2_optional = QLabel("* optional")

        self.label1 = QLabel()
        self.label1.setText("Add Backup Data")
        self.label1.setStyleSheet("font-weight: bold ; font-size: 15px")

        self.label2 = QLabel()
        self.label2.setText("Add REST Data")
        self.label2.setStyleSheet("font-weight: bold ; font-size: 15px")

        #####################
        # Line Edits
        #####################

        self.input_backup_name = QLineEdit(str(""), self)
        self.input_repository = QLineEdit(str(""), self)
        self.input_source = QLineEdit(str(""), self)
        self.input_password = QLineEdit(str(""), self)
        self.input_exclude_list = QLineEdit(str(""), self)

        # REST
        self.rest_domain = QLineEdit(str(""), self)
        self.rest_domain.setPlaceholderText('rest.DOMAIN.com')

        self.rest_port = QLineEdit(str(""), self)
        self.rest_port.setPlaceholderText('8000')

        self.rest_user = QLineEdit(str(""), self)
        self.rest_user.setPlaceholderText('HTTP User')

        self.rest_password = QLineEdit(str(""), self)
        self.rest_password.setPlaceholderText('HTTP Password')

        self.rest_folder = QLineEdit(str(""), self)
        self.rest_folder.setPlaceholderText('my_backup')

        # Declare input field as password field with (*)
        # self.input_password.setEchoMode(QLineEdit.Password)
        # self.input_exclude_list = QLineEdit(str(""), self)

        self.input_repository.setReadOnly(True)
        self.input_source.setReadOnly(True)
        self.input_exclude_list.setReadOnly(True)

        #####################
        # Check Boxes
        #####################
        self.checkbox_rest = QCheckBox("REST ?")

        #####################
        # RegEx
        #####################

        reg_ex = QtCore.QRegularExpression('^[A-Za-z0-9-_>]{16}$')
        input_validator = QRegularExpressionValidator(reg_ex, self.input_backup_name)
        self.input_backup_name.setValidator(input_validator)

        reg_ex = QtCore.QRegularExpression('^[A-Za-z0-9-_>@*]{48}$')
        input_validator = QRegularExpressionValidator(reg_ex, self.input_password)
        self.input_password.setValidator(input_validator)

        rx = QtCore.QRegularExpression('^[0-9]{5}$')
        v = QRegularExpressionValidator(rx, self)
        self.rest_port.setValidator(v)

        #####################
        # Layout
        #####################

        # Create layout
        self.layout = QFormLayout()

        self.tabs = QTabWidget()
        self.tab_edit_backup_data = QWidget()
        self.tab_edit_rest_data = QWidget()

        self.tabs.addTab(self.tab_edit_backup_data, "Backup Data")
        self.tabs.addTab(self.tab_edit_rest_data, "REST Data")

        #####################
        # tab_edit_rest_data
        #####################

        self.tab_edit_backup_data.layout = QGridLayout()

        self.tab_edit_backup_data.layout.addWidget(self.label1, 0, 0)

        self.tab_edit_backup_data.layout.addWidget(self.label1_empty_line, 1, 0)

        self.tab_edit_backup_data.layout.addWidget(self.checkbox_rest, 1, 1)
        self.checkbox_rest.stateChanged.connect(checkbox_changed)

        self.tab_edit_backup_data.layout.addWidget(QLabel("Backup Name"), 2, 0)
        self.tab_edit_backup_data.layout.addWidget(self.input_backup_name, 2, 1)

        self.tab_edit_backup_data.layout.addWidget(QLabel("Repository"), 3, 0)
        self.tab_edit_backup_data.layout.addWidget(self.button_get_repository, 3, 3)
        self.tab_edit_backup_data.layout.addWidget(self.input_repository, 3, 1)

        self.tab_edit_backup_data.layout.addWidget(QLabel("Source"), 4, 0)
        self.tab_edit_backup_data.layout.addWidget(self.button_get_source_path, 4, 3)
        self.tab_edit_backup_data.layout.addWidget(self.input_source, 4, 1)

        self.tab_edit_backup_data.layout.addWidget(QLabel("Password"), 5, 0)
        self.tab_edit_backup_data.layout.addWidget(self.input_password, 5, 1)

        self.tab_edit_backup_data.layout.addWidget(QLabel("Exclude List *"), 6, 0)
        self.tab_edit_backup_data.layout.addWidget(self.button_get_exclude_list, 6, 3)
        self.tab_edit_backup_data.layout.addWidget(self.input_exclude_list, 6, 1)

        self.tab_edit_backup_data.layout.addWidget(self.label1_optional, 7, 0)

        self.tab_edit_backup_data.layout.addWidget(self.label4_empty_line, 8, 0)

        self.tab_edit_backup_data.layout.addWidget(self.button2, 9, 0)

        self.tab_edit_backup_data.layout.addWidget(self.action_quit, 9, 3)

        self.tab_edit_backup_data.setLayout(self.tab_edit_backup_data.layout)

        #####################
        # tab_edit_rest_data
        #####################

        self.tab_edit_rest_data.layout = QGridLayout()

        self.tab_edit_rest_data.layout.addWidget(self.label2, 0, 0)

        self.tab_edit_rest_data.layout.addWidget(QLabel("REST Domain "), 2, 0)
        self.tab_edit_rest_data.layout.addWidget(self.rest_domain, 2, 1)

        self.tab_edit_rest_data.layout.addWidget(QLabel("REST Port"), 3, 0)
        self.tab_edit_rest_data.layout.addWidget(self.rest_port, 3, 1)

        self.tab_edit_rest_data.layout.addWidget(QLabel("REST User"), 4, 0)
        self.tab_edit_rest_data.layout.addWidget(self.rest_user, 4, 1)

        self.tab_edit_rest_data.layout.addWidget(QLabel("REST Password"), 5, 0)
        self.tab_edit_rest_data.layout.addWidget(self.rest_password, 5, 1)

        self.tab_edit_rest_data.layout.addWidget(QLabel("REST Folder"), 6, 0)
        self.tab_edit_rest_data.layout.addWidget(self.rest_folder, 6, 1)

        self.tab_edit_rest_data.layout.addWidget(self.label5_empty_line, 7, 0)

        self.tab_edit_rest_data.layout.addWidget(self.label5_empty_line, 8, 0)

        self.tab_edit_rest_data.layout.addWidget(self.button_save_tab_restic, 10, 0)

        self.tab_edit_rest_data.layout.addWidget(self.action_quit_2, 10, 3)

        self.tab_edit_rest_data.setLayout(self.tab_edit_rest_data.layout)

        # Tab is turned off at startup. Activated via the 'checkbox_rest' button.
        self.tabs.setTabEnabled(1, False)

        self.layout.addWidget(self.tabs)

        self.setLayout(self.layout)

        if ini.settings.value('Exclude_List'):
            self.input_exclude_list.setText(ini.settings.value('Exclude_List'))


###############################################
# Fourth Window / Edit Backup
###############################################


class EditBackupWindow(QWidget):
    """
    """

    def __init__(self):
        super().__init__()

        self.in1 = None
        self.in5 = None

        ###############################################
        # Save Backup
        ###############################################

        def save_backup():
            """
            """
            # check for loaded data
            row = ActiveRow.get(self)

            if row == -1:
                msg_box_error(self, "Please load data before add!")
                return False

            else:

                read_lineedit()

                # Without backup name, we can't store the entry!
                if self.in1 == "":
                    msg_box_error(self, "You must set a name!")
                    return

                # clear keys_list
                keys_list = []

                # redo keys_list
                for key in backups:
                    keys_list.append(key)
                # print("NEW KEY LIST! keys_list", keys_list)

                # What do we do here? We get the keys in backups.
                # print(key_list)
                # Output: ['0', '1', '2', '3', '5', '6']

                # [keys_list[row]] means e.g. with row = 1
                # We edit the entry with the number 1 in backups

                # Example
                # print(keys_list[4]) // fourth entry of the above list, makes the value 5
                # Output: 5

                # Example
                # print(backups[keys_list[0]]['name']) // fetches the name of the 1st entry in backups
                # Output: Example1

                # Example
                # print(backups[keys_list[0]])

                # Output: {'name': 'Example1', 'repository': '/home/frank/backup1',
                # 'source': '/home/frank/source1', 'password': '1234', 'init': '0', 'exclude': ''}

                # We set the new data that we fetched with read_lineedit().

                if self.in1 and self.in3:
                    backups[keys_list[row]] = {'name': self.in1,
                                               'repository': self.in2,
                                               'source': self.in3,
                                               'password': self.in4,
                                               'init': self.in6,
                                               'exclude': self.in5,
                                               'rest': self.in7,
                                               'rest_domain': self.in8,
                                               'rest_port': self.in9,
                                               'rest_user': self.in10,
                                               'rest_password': self.in11,
                                               'rest_folder': self.in12}
                else:
                    msg_box(self, "Please fill in all three fields!")
                    mainWin.statusBar().showMessage(backup_data[row].name)
                    return False

                # load JSON
                BackupList.save_json(self)

                # UI
                mainWin.widget.setPlainText("")

                mainWin.statusBar().showMessage("")

                # We redo the classes objects
                BackupList.objects_edit(self)

                # Load widget with new data!
                ActiveRow.click(self, row)

                # we set active row in listWidget
                mainWin.listWidget.setCurrentRow(row)

                close()

        ###############################################
        # get Home_Path
        ###############################################

        def get_repository_path():
            """get repository path"""
            path = get_path(self, 'Home_Path', 'Repository Path')

            if not path:
                return False
            else:
                self.input_repository.setText(path)

        ###############################################
        # get Source_Path
        ###############################################

        def get_source_path():
            """get source_path"""
            path = get_path(self, 'Source_Path', 'Source Path')

            if not path:
                return False
            else:
                self.input_source.setText(path)

        ###############################################
        # get_exclude_list
        ###############################################

        def get_exclude_list():
            """get exclude list"""
            path = get_file_path(self, 'Exclude_List', 'Exclude List')

            if path[0] == "":
                return False
            else:
                self.input_exclude_list.setText(path[0])

        ###############################################
        # read line-edits
        ###############################################

        def read_lineedit():
            """Here we read the objects of the line edits directly and store their content in a variable."""

            self.in1 = str(self.input_backup_name.text())  # Backup name
            self.in2 = str(self.input_repository.text())  # repository
            self.in3 = str(self.input_source.text())  # source
            self.in4 = str(self.input_password.text())  # password
            self.in5 = str(self.input_exclude_list.text())  # exclude
            self.in6 = str(self.status_init.text())  # init
            self.in7 = str(self.status_rest.text())  # REST
            self.in8 = str(self.rest_domain.text())
            self.in9 = str(self.rest_port.text())
            self.in10 = str(self.rest_user.text())
            self.in11 = str(self.rest_password.text())
            self.in12 = str(self.rest_folder.text())

        ###############################################
        # close()
        ###############################################

        def close():
            """
            """
            self.tab_edit_rest_data.setEnabled(False)
            self.close()

        ###############################################
        # Functions for delete input field
        ###############################################

        def del_repo():
            """
            """
            self.input_repository.clear()

        def del_source():
            """
            """
            self.input_source.clear()

        def del_exclude():
            """
            """
            self.input_exclude_list.clear()

        ###############################################
        # Window Definition1
        ###############################################

        self.title = 'Restic UI - Edit Backup'
        self.setWindowTitle(self.title)
        self.setWindowModality(Qt.WindowModality.ApplicationModal)
        self.setWindowFlag(Qt.WindowType.WindowCloseButtonHint, False)
        self.setWindowFlag(Qt.WindowType.WindowMinimizeButtonHint, False)
        self.setMinimumSize(600, 400)
        self.setMaximumSize(600, 400)

        #####################
        # Button
        #####################

        # Save (Backup Tab)
        self.button_save_tab_backup = QPushButton("", self)
        self.button_save_tab_backup.setIcon(QIcon.fromTheme(
            'document-save', QIcon(ini.CWD + "/icons/document-save")))
        self.button_save_tab_backup.setToolTip("Save data")
        self.button_save_tab_backup.clicked.connect(save_backup)
        self.button_save_tab_backup.clicked.connect(close)

        # Get Repository Path
        self.button_get_repository = QPushButton("", self)
        self.button_get_repository.setIcon(QIcon.fromTheme(
            'document-open-folder', QIcon(ini.CWD + "/icons/folder-open")))
        self.button_get_repository.clicked.connect(get_repository_path)

        # Get Source Path
        self.button_get_source_path = QPushButton("", self)
        self.button_get_source_path.setIcon(QIcon.fromTheme(
            'document-open-folder', QIcon(ini.CWD + "/icons/folder-open")))
        self.button_get_source_path.clicked.connect(get_source_path)

        # Exclude List
        self.button_get_exclude_list = QPushButton("", self)
        self.button_get_exclude_list.setIcon(QIcon.fromTheme(
            'document-open-folder', QIcon(ini.CWD + "/icons/folder-open")))
        self.button_get_exclude_list.clicked.connect(get_exclude_list)

        # Quit (Backup Tab)
        self.action_quit = QPushButton("", self)
        self.action_quit.setIcon(QIcon.fromTheme(
            'application-exit', QIcon(ini.CWD + "/icons/exit")))
        self.action_quit.setToolTip("Quit without saving")
        self.action_quit.clicked.connect(close)

        # Save Backup (REST Tab)
        self.button_save_tab_restic = QPushButton("", self)
        self.button_save_tab_restic.setIcon(QIcon.fromTheme(
            'document-save', QIcon(ini.CWD + "/icons/document-save")))
        self.button_save_tab_restic.setToolTip("Save data")
        self.button_save_tab_restic.clicked.connect(save_backup)
        self.button_save_tab_restic.clicked.connect(close)

        # Quit (REST tab)
        self.action_quit_2 = QPushButton("", self)
        self.action_quit_2.setIcon(QIcon.fromTheme(
            'application-exit', QIcon(ini.CWD + "/icons/exit")))
        self.action_quit_2.setToolTip("Quit without saving")
        self.action_quit_2.clicked.connect(close)

        # Clear
        self.button_del_repository = QPushButton("", self)
        self.button_del_repository.setIcon(QIcon.fromTheme(
            'edit-reset', QIcon(ini.CWD + "/icons/edit-clear")))
        self.button_del_repository.clicked.connect(del_repo)

        # Clear
        self.button_del_source = QPushButton("", self)
        self.button_del_source.setIcon(QIcon.fromTheme(
            'edit-reset', QIcon(ini.CWD + "/icons/edit-clear")))
        self.button_del_source.clicked.connect(del_source)

        # Clear
        self.button_del_exclude = QPushButton("", self)
        self.button_del_exclude.setIcon(QIcon.fromTheme(
            'edit-reset', QIcon(ini.CWD + "/icons/edit-clear")))
        self.button_del_exclude.clicked.connect(del_exclude)

        #####################
        # Font etc.
        #####################

        self.label_edit_backup_data = QLabel()
        self.label_edit_backup_data.setText("Edit Backup Data")
        self.label_edit_backup_data.setStyleSheet("font-weight: bold ; font-size: 15px")

        self.label_edit_rest_data = QLabel()
        self.label_edit_rest_data.setText("Edit REST Data")
        self.label_edit_rest_data.setStyleSheet("font-weight: bold ; font-size: 15px")

        # Create empty lines as spacers.
        self.label1_empty_line = QLabel()
        self.label2_empty_line = QLabel()
        self.label3_empty_line = QLabel()
        self.label4_empty_line = QLabel()
        self.label1_optional = QLabel("* optional")
        self.label5_empty_line = QLabel()
        self.label2_optional = QLabel("* optional")

        #####################
        # Line Edits
        #####################

        # tab_edit_backup_data Backup Data
        self.input_backup_name = QLineEdit(str(""), self)  # reg_ex
        # self.input_backup_name.textChanged.connect(test)
        self.input_backup_name.setToolTip("Allowed characters (A-Za-z0-9-_>@) max. 16")

        self.input_repository = QLineEdit(str(""), self)
        self.input_repository.setReadOnly(True)

        self.input_source = QLineEdit(str(""), self)
        self.input_source.setReadOnly(True)

        self.input_password = QLineEdit(str(""), self)  # reg_ex
        self.input_password.setToolTip("Allowed characters (A-Za-z0-9-_>@) max. 16")

        self.input_exclude_list = QLineEdit(str(""), self)
        self.input_exclude_list.setReadOnly(True)

        self.status_init = QLineEdit(str(""), self)
        self.status_init.setReadOnly(True)
        self.status_init.setFixedWidth(15)

        self.status_rest = QLineEdit(str(""), self)
        self.status_rest.setReadOnly(True)
        self.status_rest.setFixedWidth(15)

        # tab_edit_rest_data REST Data
        self.rest_domain = QLineEdit(str(""), self)
        self.rest_domain.setPlaceholderText('rest.DOMAIN.com')

        self.rest_port = QLineEdit(str(""), self)  # reg_ex
        self.rest_port.setPlaceholderText('8000')
        self.rest_port.setToolTip("Allowed characters (0-9) max. 5")

        self.rest_user = QLineEdit(str(""), self)
        self.rest_user.setPlaceholderText('HTTP User')

        self.rest_password = QLineEdit(str(""), self)
        self.rest_password.setPlaceholderText('HTTP Password')

        self.rest_folder = QLineEdit(str(""), self)
        self.rest_folder.setPlaceholderText('my_backup')

        # Declare input field as password field with (*)
        # self.input_password.setEchoMode(QLineEdit.Password)
        # self.input_exclude_list = QLineEdit(str(""), self)

        #####################
        # RegEx
        #####################

        reg_ex = QtCore.QRegularExpression('^[A-Za-z0-9-_>@]{16}$')
        input_validator = QRegularExpressionValidator(reg_ex, self.input_backup_name)
        self.input_backup_name.setValidator(input_validator)

        reg_ex2 = QtCore.QRegularExpression('^[A-Za-z0-9-_>@]{16}$')
        input_validator2 = QRegularExpressionValidator(reg_ex2, self.input_password)
        self.input_password.setValidator(input_validator2)

        reg_ex3 = QtCore.QRegularExpression('^[0-9]{5}$')
        input_validator3 = QRegularExpressionValidator(reg_ex3, self.rest_port)
        self.rest_port.setValidator(input_validator3)

        rx = QtCore.QRegularExpression('^[0-9]{5}$')
        v = QRegularExpressionValidator(rx, self)
        self.rest_port.setValidator(v)

        #####################
        # Layout
        #####################

        # Create layout
        self.layout = QGridLayout()

        self.tabs = QTabWidget()
        self.tab_edit_backup_data = QWidget()
        self.tab_edit_rest_data = QWidget()

        self.tabs.addTab(self.tab_edit_backup_data, "Backup Data")
        self.tabs.addTab(self.tab_edit_rest_data, "REST Data")

        #####################
        # tab_edit_backup_data Backup Data
        #####################

        self.tab_edit_backup_data.layout = QGridLayout()

        self.tab_edit_backup_data.layout.addWidget(self.label_edit_backup_data, 0, 0)

        self.tab_edit_backup_data.layout.addWidget(self.label1_empty_line, 1, 0)

        self.tab_edit_backup_data.layout.addWidget(QLabel("Backup Name"), 2, 0)
        self.tab_edit_backup_data.layout.addWidget(self.input_backup_name, 2, 1)

        self.tab_edit_backup_data.layout.addWidget(QLabel("Repository"), 3, 0)
        self.tab_edit_backup_data.layout.addWidget(self.button_del_repository, 3, 2)
        self.tab_edit_backup_data.layout.addWidget(self.button_get_repository, 3, 3)
        self.tab_edit_backup_data.layout.addWidget(self.input_repository, 3, 1)

        self.tab_edit_backup_data.layout.addWidget(QLabel("Source"), 4, 0)
        self.tab_edit_backup_data.layout.addWidget(self.button_del_source, 4, 2)
        self.tab_edit_backup_data.layout.addWidget(self.button_get_source_path, 4, 3)
        self.tab_edit_backup_data.layout.addWidget(self.input_source, 4, 1)

        self.tab_edit_backup_data.layout.addWidget(QLabel("Password"), 5, 0)
        self.tab_edit_backup_data.layout.addWidget(self.input_password, 5, 1)

        self.tab_edit_backup_data.layout.addWidget(QLabel("Exclude List *"), 6, 0)
        self.tab_edit_backup_data.layout.addWidget(self.button_del_exclude, 6, 2)
        self.tab_edit_backup_data.layout.addWidget(self.button_get_exclude_list, 6, 3)
        self.tab_edit_backup_data.layout.addWidget(self.input_exclude_list, 6, 1)

        self.tab_edit_backup_data.layout.addWidget(QLabel("Init"), 7, 0)
        self.tab_edit_backup_data.layout.addWidget(self.status_init, 7, 1)

        self.tab_edit_backup_data.layout.addWidget(QLabel("REST"), 8, 0)
        self.tab_edit_backup_data.layout.addWidget(self.status_rest, 8, 1)

        self.tab_edit_backup_data.layout.addWidget(self.label1_optional, 9, 0)

        self.tab_edit_backup_data.layout.addWidget(self.label4_empty_line, 10, 0)

        self.tab_edit_backup_data.layout.addWidget(self.button_save_tab_backup, 11, 0)

        self.tab_edit_backup_data.layout.addWidget(self.action_quit, 11, 2)

        self.tab_edit_backup_data.setLayout(self.tab_edit_backup_data.layout)

        #####################
        # tab_edit_rest_data REST Data
        #####################

        self.tab_edit_rest_data.layout = QGridLayout()

        self.tab_edit_rest_data.layout.addWidget(self.label_edit_rest_data, 0, 0)

        self.tab_edit_rest_data.layout.addWidget(QLabel("REST Domain "), 2, 0)
        self.tab_edit_rest_data.layout.addWidget(self.rest_domain, 2, 1)

        self.tab_edit_rest_data.layout.addWidget(QLabel("REST Port"), 3, 0)
        self.tab_edit_rest_data.layout.addWidget(self.rest_port, 3, 1)

        self.tab_edit_rest_data.layout.addWidget(QLabel("REST User"), 4, 0)
        self.tab_edit_rest_data.layout.addWidget(self.rest_user, 4, 1)

        self.tab_edit_rest_data.layout.addWidget(QLabel("REST Password"), 5, 0)
        self.tab_edit_rest_data.layout.addWidget(self.rest_password, 5, 1)

        self.tab_edit_rest_data.layout.addWidget(QLabel("REST Folder"), 6, 0)
        self.tab_edit_rest_data.layout.addWidget(self.rest_folder, 6, 1)

        self.tab_edit_rest_data.layout.addWidget(self.label5_empty_line, 7, 0)

        self.tab_edit_rest_data.layout.addWidget(self.label5_empty_line, 8, 0)

        self.tab_edit_rest_data.layout.addWidget(self.button_save_tab_restic, 11, 0)

        self.tab_edit_rest_data.layout.addWidget(self.action_quit_2, 11, 2)

        self.tab_edit_rest_data.setLayout(self.tab_edit_rest_data.layout)

        # Tab is turned off at startup. Activated via the 'checkbox_rest' button.
        # self.tabs.setTabEnabled(1, False)
        self.tab_edit_rest_data.setEnabled(False)
        self.layout.addWidget(self.tabs)

        self.setLayout(self.layout)


###############################################
# Main Window
###############################################

class MainWindow(QMainWindow):
    """
    """

    def __init__(self):
        super().__init__()

        "First Thread with documentation"
        # Worker for restic_stats
        # Create a QThread object
        self.thread_stats = QThread()
        # Create a worker object
        self.worker_stats = WorkerResticStats()
        # Move worker to the thread
        self.worker_stats.moveToThread(self.thread_stats)
        # Connect signals and slots
        self.thread_stats.started.connect(self.worker_stats.run)
        self.worker_stats.stats_finished[str].connect(self.restic_stats_finished)
        self.worker_stats.stats_error[str].connect(self.restic_stats_error)
        "First thread end"

        # Worker for restic_backup
        self.thread_backup = QThread()
        self.worker_backup = WorkerResticBackup()
        self.worker_backup.moveToThread(self.thread_backup)
        self.thread_backup.started.connect(self.worker_backup.run)
        self.worker_backup.backup_finished[str].connect(self.restic_backup_finished)
        self.worker_backup.backup_error[str].connect(self.restic_backup_error)

        # Worker for restic_snapshots
        self.thread_snapshots = QThread()
        self.worker_snapshots = WorkerResticSnapshots()
        self.worker_snapshots.moveToThread(self.thread_snapshots)
        self.thread_snapshots.started.connect(self.worker_snapshots.run)
        self.worker_snapshots.snapshots_finished[str].connect(self.restic_snapshots_finished)
        self.worker_snapshots.snapshots_error[str].connect(self.restic_snapshots_error)

        # Worker for list snapshot
        self.thread_list_snapshot = QThread()
        self.worker_list_snapshot = WorkerResticListSnapshot()
        self.worker_list_snapshot.moveToThread(self.thread_list_snapshot)
        self.thread_list_snapshot.started.connect(self.worker_list_snapshot.run)
        self.worker_list_snapshot.ls2_finished[str].connect(self.restic_ls2_finished)
        self.worker_list_snapshot.ls2_error[str].connect(self.restic_ls2_error)

        # Worker for restic_init
        self.thread_init = QThread()
        self.worker_init = WorkerResticInit()
        self.worker_init.moveToThread(self.thread_init)
        self.thread_init.started.connect(self.worker_init.run)
        self.worker_init.init_finished[str].connect(self.restic_init_finished)
        self.worker_init.init_error[str].connect(self.restic_init_error)

        # Worker for restic_prune
        self.thread_prune = QThread()
        self.worker_prune = WorkerResticPrune()
        self.worker_prune.moveToThread(self.thread_prune)
        self.thread_prune.started.connect(self.worker_prune.run)
        self.worker_prune.prune_finished[str].connect(self.restic_prune_finished)
        self.worker_prune.prune_error[str].connect(self.restic_prune_error)

        # Worker for restic_check
        self.thread_check = QThread()
        self.worker_check = WorkerResticCheck()
        self.worker_check.moveToThread(self.thread_check)
        self.thread_check.started.connect(self.worker_check.run)
        self.worker_check.check_finished[str].connect(self.restic_check_finished)
        self.worker_check.check_error[str].connect(self.restic_check_error)

        # Worker for restic_unlock
        self.thread_unlock = QThread()
        self.worker_unlock = WorkerResticUnlock()
        self.worker_unlock.moveToThread(self.thread_unlock)
        self.thread_unlock.started.connect(self.worker_unlock.run)
        self.worker_unlock.unlock_finished[str].connect(self.restic_unlock_finished)
        self.worker_unlock.unlock_error[str].connect(self.restic_unlock_error)

        # Worker for restic_restore
        self.thread_restore = QThread()
        self.worker_restore = WorkerResticRestore()
        self.worker_restore.moveToThread(self.thread_restore)
        self.thread_restore.started.connect(self.worker_restore.run)
        self.worker_restore.restore_finished[str].connect(self.restic_restore_finished)
        self.worker_restore.restore_error[str].connect(self.restic_restore_error)

        # Worker for restic_mount
        self.thread_mount = QThread()
        self.worker_mount = WorkerResticMount()
        self.worker_mount.moveToThread(self.thread_mount)
        self.thread_mount.started.connect(self.worker_mount.run)
        self.worker_mount.mount_finished[str].connect(self.restic_mount_finished)
        self.worker_mount.mount_error[str].connect(self.restic_mount_error)

        # Worker for restic_cache
        self.thread_cache = QThread()
        self.worker_cache = WorkerResticClearCache()
        self.worker_cache.moveToThread(self.thread_cache)
        self.thread_cache.started.connect(self.worker_cache.run)
        self.worker_cache.cache_finished[str].connect(self.restic_cache_finished)
        self.worker_cache.cache_error[str].connect(self.restic_cache_error)

        # Worker for restic_migrate -r Check!
        self.thread_migrate_check = QThread()
        self.worker_migrate_check = WorkerResticCheckMigrate()
        self.worker_migrate_check.moveToThread(self.thread_migrate_check)
        self.thread_migrate_check.started.connect(self.worker_migrate_check.run)
        self.worker_migrate_check.migrate_check_finished[str].connect(self.restic_migrate_check_finished)
        self.worker_migrate_check.migrate_check_error[str].connect(self.restic_migrate_check_error)

        # Worker for restic_migrate upgrade_repo_v2
        self.thread_migrate_update = QThread()
        self.worker_migrate_update = WorkerResticMigrateUpdate()
        self.worker_migrate_update.moveToThread(self.thread_migrate_update)
        self.thread_migrate_update.started.connect(self.worker_migrate_update.run)
        self.worker_migrate_update.migrate_update_finished[str].connect(self.restic_migrate_update_finished)
        self.worker_migrate_update.migrate_update_error[str].connect(self.restic_migrate_update_error)

        # Worker for call repo version, v1 or v2 ?
        self.thread_call_repo_version = QThread()
        self.worker_call_repo_version = WorkerResticCallRepoVersion()
        self.worker_call_repo_version.moveToThread(self.thread_call_repo_version)
        self.thread_call_repo_version.started.connect(self.worker_call_repo_version.run)
        self.worker_call_repo_version.call_repo_version_finished[str].connect(self.call_repo_version_finished)
        self.worker_call_repo_version.call_repo_version_error[str].connect(self.call_repo_version_error)

        ###############################################
        # Set start parameter .isEnabled
        ###############################################

        # Set start parameter .isEnabled for menu Tools/Snapshots and Tools/List Snapshot
        self.menu_is_enabled1 = True
        self.menu_is_enabled2 = False
        self.menu_is_enabled3 = False

        ###############################################
        # Window Definition
        ###############################################

        self.title = 'Restic UI'
        self.window_add_backup = AddBackupWindow()
        self.window_edit_backup = EditBackupWindow()
        self.setWindowTitle(self.title)
        self.setMinimumSize(800, 600)

        ###############################################
        # QDockWidget
        ###############################################

        self.setCentralWidget(QTextEdit())

        # QDock Widget
        self.items = QDockWidget("Backup List", self)
        self.items.setAllowedAreas(Qt.DockWidgetArea.RightDockWidgetArea)
        self.items.setFloating(False)

        # LIST WIDGET
        self.listWidget = QListWidget()
        self.listWidget.resize(300, 120)

        # Add listWidget to QDockWidget
        self.items.setWidget(self.listWidget)

        # QDockWidget Set position and content
        self.addDockWidget(Qt.DockWidgetArea.RightDockWidgetArea, self.items)

        # Signale
        self.listWidget.itemClicked.connect(self.clicked)

        ###############################################
        # ToolBar start
        ###############################################

        self.toolbar = QToolBar("My main toolbar")
        self.toolbar.setIconSize(QSize(32, 32))
        self.addToolBar(self.toolbar)

        toolbar_load_JSON = QAction(QIcon.fromTheme('view-refresh', QIcon(
            ini.CWD + "/icons/stock_reload")), "Reload JSON", self)
        toolbar_load_JSON.triggered.connect(BackupList.load_json)
        self.toolbar.addAction(toolbar_load_JSON)

        toolbar_start_backup = QAction(QIcon.fromTheme("media-playback-start", QIcon(
            ini.CWD + "/icons/media-playback-start")), "Start BACKUP", self)
        toolbar_start_backup.triggered.connect(self.restic_backup)
        self.toolbar.addAction(toolbar_start_backup)

        toolbar_spare = QAction("", self)
        toolbar_spare.setEnabled(False)
        self.toolbar.addAction(toolbar_spare)

        self.toolbar_restic_test = QToolButton()
        self.toolbar_restic_test.setCheckable(False)
        self.toolbar_restic_test.clicked.connect(self.restic_switch)
        self.toolbar.addWidget(self.toolbar_restic_test)

        if ini.settings.value('Restic_Test_Version') == "1":
            self.toolbar_restic_test.setText("Restic Dev Version")
            self.toolbar_restic_test.setStyleSheet("color: red ; font-weight: bold ; font-size: 15px")

        else:
            self.toolbar_restic_test.setText("Restic Version")
            self.toolbar_restic_test.setStyleSheet("color: black ; font-weight: italic ; font-size: 15px")

        ###############################################
        # Menu Start
        ###############################################

        # ----Main Menu ---- #
        main_menu = self.menuBar()
        file_menu = main_menu.addMenu('&File')
        restic_menu = main_menu.addMenu('&Restic')
        tools_menu = main_menu.addMenu('&Tools')
        migrate_menu = main_menu.addMenu('&Migrate')
        help_menu = main_menu.addMenu('&Help')

        # ----Help Menu ---- #
        about_button = QAction(QIcon.fromTheme(
            'help-about', QIcon(ini.CWD + "/icons/help-info")), 'About Restic UI', self)
        about_button.setShortcut('Ctrl+A')
        about_button.triggered.connect(self.button_about)
        help_menu.addAction(about_button)

        # ----File Menu ---- #
        load_JSON = QAction(QIcon.fromTheme('view-refresh', QIcon(
            ini.CWD + "/icons/reload")), 'Load File', self)
        load_JSON.triggered.connect(BackupList.load_json)
        load_JSON.setShortcut('Ctrl+L')
        file_menu.addAction(load_JSON)

        open_repo = QAction(QIcon.fromTheme('document-open-folder', QIcon(
            ini.CWD + "/icons/folder-open")), 'Open Repo', self)
        open_repo.triggered.connect(self.get_repo_path)
        open_repo.setShortcut('Ctrl+O')
        file_menu.addAction(open_repo)

        add_JSON = QAction(QIcon.fromTheme('cursor-cross', QIcon(
            ini.CWD + "/icons/add")), 'Add Backup', self)
        add_JSON.triggered.connect(self.show_add_window)
        add_JSON.setShortcut('Ctrl+A')
        file_menu.addAction(add_JSON)

        self.edit_JSON = QAction(QIcon.fromTheme('document-edit', QIcon(
            ini.CWD + "/icons/edit")), 'Edit Backup', self)
        self.edit_JSON.triggered.connect(self.fill_triggered)
        self.edit_JSON.setShortcut('Ctrl+E')
        file_menu.addAction(self.edit_JSON)

        del_JSON = QAction(QIcon.fromTheme(
            'edit-delete', QIcon(ini.CWD + "/icons/delete")), 'Delete Backup', self)
        del_JSON.triggered.connect(self.del_entry)
        del_JSON.setShortcut('Del')
        file_menu.addAction(del_JSON)

        file_menu.addSeparator()

        exit_app = QAction(QIcon.fromTheme('application-exit', QIcon(
            ini.CWD + "/icons/exit")), 'Exit', self)
        exit_app.triggered.connect(self.close_event)
        exit_app.setShortcut('Ctrl+X')
        file_menu.addAction(exit_app)

        # ----Restic Menu ---- #
        restic_init = QAction(QIcon(), 'Init', self)
        restic_init.triggered.connect(self.restic_init)
        restic_init.setStatusTip('Initialize a new repository')
        restic_menu.addAction(restic_init)

        restic_backup = QAction(QIcon(), 'Backup', self)
        restic_backup.triggered.connect(self.restic_backup)
        restic_backup.setStatusTip(
            'Create a new backup of files and/or directories')
        restic_menu.addAction(restic_backup)

        restic_mount = QAction(QIcon(), 'Mount', self)
        restic_mount.triggered.connect(self.restic_mount)
        restic_mount.setStatusTip('Mount the repository')
        restic_menu.addAction(restic_mount)

        restic_umount = QAction(QIcon(), 'UMount', self)
        restic_umount.triggered.connect(self.restic_umount)
        restic_umount.setStatusTip('UMount the repository')
        restic_menu.addAction(restic_umount)

        restic_restore = QAction(QIcon(), 'Restore', self)
        restic_restore.triggered.connect(self.restic_restore)
        restic_restore.setStatusTip('Extract the data from a snapshot')
        restic_menu.addAction(restic_restore)

        # ----Tools Menu ---- #
        self.restic_list = QAction(QIcon(), 'Snapshots', self)
        self.restic_list.triggered.connect(self.restic_snapshots)
        self.restic_list.setStatusTip('List all snapshots')
        self.restic_list.triggered.connect(
            self.menu_restic_snapshots_triggered)
        self.restic_list.setEnabled(self.menu_is_enabled1)
        tools_menu.addAction(self.restic_list)

        self.restic_ls = QAction(QIcon(), 'List Snapshot', self)
        self.restic_ls.triggered.connect(self.restic_ls2)
        self.restic_ls.setStatusTip('List files in a snapshot')
        self.restic_ls.triggered.connect(self.menu_restic_ls2_triggered)
        self.restic_ls.setEnabled(self.menu_is_enabled2)
        tools_menu.addAction(self.restic_ls)

        restic_check = QAction(QIcon(), 'Check', self)
        restic_check.triggered.connect(self.restic_check)
        restic_check.setStatusTip('Check the repository for errors')
        tools_menu.addAction(restic_check)

        restic_unlock = QAction(QIcon(), 'Unlock', self)
        restic_unlock.triggered.connect(self.restic_unlock)
        restic_unlock.setStatusTip('Remove locks other processes created')
        tools_menu.addAction(restic_unlock)

        restic_stats = QAction(QIcon(), 'Stats', self)
        restic_stats.triggered.connect(self.restic_stats)
        restic_stats.setStatusTip(
            'Scan the repository and show basic statistics')
        tools_menu.addAction(restic_stats)

        restic_prune = QAction(QIcon(), 'Prune', self)
        restic_prune.triggered.connect(self.restic_prune)
        restic_prune.setStatusTip('Remove unneeded data from the repository')
        tools_menu.addAction(restic_prune)

        restic_version = QAction(QIcon(), 'Version', self)
        restic_version.triggered.connect(self.restic_version)
        restic_version.setStatusTip('Print version information')
        tools_menu.addAction(restic_version)

        restic_cache = QAction(QIcon(), 'Cache  --cleanup', self)
        restic_cache.triggered.connect(self.restic_cache)
        restic_cache.setStatusTip('Cleanup cache')
        tools_menu.addAction(restic_cache)

        self.settings = QAction(QIcon.fromTheme('settings-configure', QIcon(
            ini.CWD + "/icons/settings")), 'Settings', self)
        self.settings.setShortcut('Ctrl+S')
        self.settings.triggered.connect(self.show_settings_window)
        self.settings.setStatusTip('Show settings window')
        tools_menu.addAction(self.settings)

        # ----Tools Menu ---- #
        restic_migrate_check = QAction(QIcon(), 'Migrate Check', self)
        restic_migrate_check.triggered.connect(self.restic_migrate_check)
        restic_migrate_check.setStatusTip('Check the repository for available migrations')
        migrate_menu.addAction(restic_migrate_check)

        restic_migrate_update = QAction(QIcon(), 'Migrate Update', self)
        restic_migrate_update.triggered.connect(self.restic_migrate_update)
        restic_migrate_update.setStatusTip('Migrate the repository to repo version 2')
        migrate_menu.addAction(restic_migrate_update)

        ###############################################
        # TextEdit
        ###############################################

        # ----QPlainTextEdit ---- #
        layout = QVBoxLayout()
        self.widget = QPlainTextEdit()
        self.widget.setReadOnly(True)
        self.widget.setLayout(layout)
        self.setCentralWidget(self.widget)

        ###############################################
        # StatusBar
        ###############################################

        self.statusBar().showMessage("Please load Data!")

    ###########################################################
    # Start Functions
    ###########################################################

    ###############################################
    # get Repository Path
    ###############################################

    def get_repo_path(self):
        """
        """
        # check for loaded data
        row = ActiveRow.get(self)

        if row == -1:
            msg_box_error(self, "Please load data before open an repo!")
            return False

        # get repo path
        path = get_path(self, 'Home_Path', 'Repository Path')

        if not path:
            return False

        # got passwd for repo
        pass_word.pw = get_password(self)  # pwd ('1234', True)
        if pass_word.pw[1] is False:
            return False

        # set restic version
        restic = ResticDev.get(self)

        # check if repo in path is valid
        try:
            args = [restic,
                    '-r',
                    path,
                    'snapshots']

            result = subprocess.run(args,
                                    input=pass_word.pw[0],
                                    check=True,
                                    capture_output=True,
                                    text=True)

        except subprocess.CalledProcessError as e:
            # Process don't successful, send signal
            msg_box_error(self, e.stderr)
            return False
        else:
            msg_box(self, result.stdout)
        finally:
            pass

        # What do we do here? We get the number of keys in backups
        # print(list(backups)[-1]) result is last key as string
        # We convert the string to INT and add +1!

        new_key = int(list(backups)[-1]) + 1

        backups[new_key] = {'name': "Open_Repo",
                            'repository': path,
                            'source': "",
                            'password': pass_word.pw[0],
                            'init': "1",
                            'exclude': "",
                            'rest': "0",
                            'rest_domain': "",
                            'rest_port': "",
                            'rest_user': "",
                            'rest_password': "",
                            'rest_folder': ""}

        mainWin.statusBar().showMessage("Please select a backup!")

        # We save the entry to the JSON
        BackupList.save_json(self)

        # UI
        mainWin.widget.setPlainText("")

        mainWin.statusBar().showMessage("")

        # We redo the classes objects
        BackupList.objects_new(self)

        # load JSON
        BackupList.load_json(self)

        last_element = len(keys_list) - 1

        ActiveRow.click(self, (last_element + 1))

        mainWin.listWidget.setCurrentRow((last_element + 1))

    ###############################################
    # Process for restic_stats is finished
    ###############################################

    @pyqtSlot(str)
    def restic_stats_finished(self, i):
        """Signal from worker thread without an error"""

        spinner.stop()

        row = self.listWidget.currentRow()

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        mainWin.widget.setPlainText(i)

        self.statusBar().showMessage(backup_data[row].name)

        self.thread_stats.quit()

    ###############################################
    # Process for restic_stats when get an error
    ###############################################

    @pyqtSlot(str)
    def restic_stats_error(self, i):
        """Signal from worker thread with an error!"""

        spinner.stop()

        msg_box_error(self, i)

        self.thread_stats.quit()

    ###############################################
    # Process for restic_backup is finished
    ###############################################

    @pyqtSlot(str)
    def restic_backup_finished(self, i):
        """Signal from worker thread without an error"""

        spinner.stop()

        row = self.listWidget.currentRow()

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        mainWin.widget.setPlainText(i)

        self.statusBar().showMessage(backup_data[row].name)

        self.thread_backup.quit()

    ###############################################
    # Process for restic_backup when get an error
    ###############################################

    @pyqtSlot(str)
    def restic_backup_error(self, i):
        """Signal from worker thread with an error!"""

        spinner.stop()

        msg_box_error(self, i)

        self.thread_backup.quit()

    ###############################################
    # Process for restic_snapshots is finished
    ###############################################

    @pyqtSlot(str)
    def restic_snapshots_finished(self, i):
        """Signal from worker thread without an error"""

        spinner.stop()

        row = self.listWidget.currentRow()

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        mainWin.widget.setPlainText(i)

        self.statusBar().showMessage(backup_data[row].name)

        self.thread_snapshots.quit()

    ###############################################
    # Process for restic_snapshots when get an error
    ###############################################

    @pyqtSlot(str)
    def restic_snapshots_error(self, i):
        """Signal from worker thread with an error!"""

        spinner.stop()

        msg_box_error(self, i)

        self.thread_snapshots.quit()

    ###############################################
    # Process for restic_ls2 is finished
    ###############################################

    @pyqtSlot(str)
    def restic_ls2_finished(self, i):
        """Signal from worker thread without an error"""

        spinner.stop()

        row = self.listWidget.currentRow()

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        mainWin.widget.setPlainText(i)

        self.statusBar().showMessage(backup_data[row].name)

        self.thread_list_snapshot.quit()

    ###############################################
    # Process for restic_ls2 when get an error
    ###############################################

    @pyqtSlot(str)
    def restic_ls2_error(self, i):
        """Signal from worker thread with an error!"""

        spinner.stop()

        msg_box_error(self, i)

        self.thread_list_snapshot.quit()

    ###############################################
    # Process for restic_init is finished
    ###############################################

    @pyqtSlot(str)
    def restic_init_finished(self, i):
        """Signal from worker thread without an error"""

        spinner.stop()

        row = self.listWidget.currentRow()

        BackupList.objects_edit(self)

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        mainWin.widget.setPlainText(i)

        self.statusBar().showMessage(backup_data[row].name)

        self.thread_init.quit()

    ###############################################
    # Process for restic_ls2 when get an error
    ###############################################

    @pyqtSlot(str)
    def restic_init_error(self, i):
        """Signal from worker thread with an error!"""

        spinner.stop()

        msg_box_error(self, i)

        self.thread_init.quit()

    ###############################################
    # Process for restic_prune is finished
    ###############################################

    @pyqtSlot(str)
    def restic_prune_finished(self, i):
        """Signal from worker thread without an error"""

        spinner.stop()

        row = self.listWidget.currentRow()

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        mainWin.widget.setPlainText(i)

        self.statusBar().showMessage(backup_data[row].name)

        self.thread_prune.quit()

    ###############################################
    # Process for restic_prune when get an error
    ###############################################

    @pyqtSlot(str)
    def restic_prune_error(self, i):
        """Signal from worker thread with an error!"""

        spinner.stop()

        msg_box_error(self, i)

        self.thread_prune.quit()

    ###############################################
    # Process for restic_check is finished
    ###############################################

    @pyqtSlot(str)
    def restic_check_finished(self, i):
        """Signal from worker thread without an error"""

        spinner.stop()

        row = self.listWidget.currentRow()

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        mainWin.widget.setPlainText(i)

        self.statusBar().showMessage(backup_data[row].name)

        self.thread_check.quit()

    ###############################################
    # Process for restic_check when get an error
    ###############################################

    @pyqtSlot(str)
    def restic_check_error(self, i):
        """Signal from worker thread with an error!"""

        spinner.stop()

        msg_box_error(self, i)

        self.thread_check.quit()

    ###############################################
    # Process for restic_unlock is finished
    ###############################################

    @pyqtSlot(str)
    def restic_unlock_finished(self, i):
        """Signal from worker thread without an error"""

        spinner.stop()

        row = self.listWidget.currentRow()

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        mainWin.widget.setPlainText(i)

        self.statusBar().showMessage(backup_data[row].name)

        self.thread_unlock.quit()

    ###############################################
    # Process for restic_unlock when get an error
    ###############################################

    @pyqtSlot(str)
    def restic_unlock_error(self, i):
        """Signal from worker thread with an error!"""

        spinner.stop()

        msg_box_error(self, i)

        self.thread_unlock.quit()

    ###############################################
    # Process for restic_restore is finished
    ###############################################

    @pyqtSlot(str)
    def restic_restore_finished(self, i):
        """Signal from worker thread without an error"""

        spinner.stop()

        row = self.listWidget.currentRow()

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        mainWin.widget.setPlainText(i)

        self.statusBar().showMessage(backup_data[row].name)

        msg_box(self, "Data successfully restored. Restore path = %s" % (res_path.restore))

        self.thread_restore.quit()

    ###############################################
    # Process for restic_restore when get an error
    ###############################################

    @pyqtSlot(str)
    def restic_restore_error(self, i):
        """Signal from worker thread with an error!"""

        spinner.stop()

        msg_box_error(self, i)

        self.thread_restore.quit()

    ###############################################
    # Process for restic_mount is finished
    ###############################################

    @pyqtSlot(str)
    def restic_mount_finished(self, i):
        """Signal from worker thread without an error"""

        spinner.stop()

        row = self.listWidget.currentRow()

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        mainWin.widget.setPlainText(i)

        self.statusBar().showMessage(backup_data[row].name)

        msg_box(self, "Data successfully mounted. Mount path = %s" % (m_path[0].path))

        self.thread_mount.quit()

    ###############################################
    # Process for restic_mount when get an error
    ###############################################

    @pyqtSlot(str)
    def restic_mount_error(self, i):
        """Signal from worker thread with an error!"""

        spinner.stop()

        msg_box_error(self, i)

        self.restic_umount()

        self.thread_mount.quit()

    ###############################################
    # Process for restic_cache is finished
    ###############################################

    @pyqtSlot(str)
    def restic_cache_finished(self, i):
        """Signal from worker thread without an error"""

        spinner.stop()

        row = self.listWidget.currentRow()

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        mainWin.widget.setPlainText(i)

        self.statusBar().showMessage(backup_data[row].name)

        msg_box(self, "Cache successfully cleanup!")

        self.thread_cache.quit()

    ###############################################
    # Process for restic_cache when get an error
    ###############################################

    @pyqtSlot(str)
    def restic_cache_error(self, i):
        """Signal from worker thread with an error!"""

        spinner.stop()

        msg_box_error(self, i)

        self.thread_cache.quit()

    ###############################################
    # Process for restic_migrate_check is finished
    ###############################################

    @pyqtSlot(str)
    def restic_migrate_check_finished(self, i):
        """ Signal from worker thread without an error """

        spinner.stop()

        row = self.listWidget.currentRow()

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        mainWin.widget.setPlainText(i)

        self.statusBar().showMessage(backup_data[row].name)

        self.thread_migrate_check.quit()

    ###############################################
    # Process for restic_migrate_check when get an error
    ###############################################

    @pyqtSlot(str)
    def restic_migrate_check_error(self, i):
        """ Signal from worker thread with an error! """

        spinner.stop()

        msg_box_error(self, i)

        self.thread_migrate_check.quit()

    ###############################################
    # Process for restic_migrate_update is finished
    ###############################################

    @pyqtSlot(str)
    def restic_migrate_update_finished(self, i):
        """ Signal from worker thread without an error """

        spinner.stop()

        if not i:
            msg_box(self, "Nothing to do.")

        row = self.listWidget.currentRow()

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        mainWin.widget.setPlainText(i)

        self.statusBar().showMessage(backup_data[row].name)

        self.thread_migrate_update.quit()

    ###############################################
    # Process for restic_migrate_update when get an error
    ###############################################

    @pyqtSlot(str)
    def restic_migrate_update_error(self, i):
        """ Signal from worker thread with an error! """

        spinner.stop()

        msg_box_error(self, i)

        self.thread_migrate_update.quit()

    ###############################################
    # Process for call_repo_version is finished
    ###############################################

    @pyqtSlot(str)
    def call_repo_version_finished(self, i):
        """ Signal from worker thread without an error """

        spinner.stop()

        row = self.listWidget.currentRow()

        # catch version from output i[54]
        repo_version = i[54]

        # when no password is saved, it's not possible
        if repo_version == '*':
            repo_version = 'Not possible without saved password'

        password = "*" * (len(backup_data[row].password))

        password_rest = "*" * (len(backup_data[row].rest_password))

        self.listWidget.setCurrentRow(row)

        self.statusBar().showMessage(backup_data[row].name)

        if os.name == "nt":
            # print("WINDOWS")
            if backup_data[row].rest == "0":
                # string = "---------------------------------------------------------------------------------------------------------------------------------------"
                text = """Backup_Name:\t\t %s\n\n Repo Version:\t\t %s\n\n Repository:\t\t %s\n\n Source:\t\t %s\n\n Password:\t\t %s\n\n Init_Status:\t\t %s\n\n Exclude List:\t\t %s\n\n""" % (
                    backup_data[row].name, repo_version, backup_data[row].repository, backup_data[row].source, password,
                    backup_data[row].init, backup_data[row].exclude)
            else:
                string = "---------------------------------------------------------------------------------------------------------------------------------------"
                text = """Backup_Name:\t\t %s\n\n Repo Version:\t\t %s\n\n Repository:\t\t %s\n\n Source:\t\t %s\n\n Password:\t\t %s\n\n Init_Status:\t\t %s\n\n Exclude List:\t\t %s\n\n %s \n\nREST:\t\t %s\n\n REST Domain:\t %s\n\n REST Port:\t\t %s\n\n REST User:\t\t %s\n\n REST Password:\t %s\n\n REST Folder:\t\t %s """ % (
                    backup_data[row].name, repo_version, backup_data[row].repository, backup_data[row].source, password,
                    backup_data[row].init, backup_data[row].exclude, string, backup_data[row].rest,
                    backup_data[row].rest_domain, backup_data[row].rest_port, backup_data[row].rest_user, password_rest,
                    backup_data[row].rest_folder)

        else:
            # print("LINUX")
            if backup_data[row].rest == "0":
                # string = "---------------------------------------------------------------------------------------------------------------------------------------"
                text = """Backup_Name:\t\t %s\n\n Repo Version:\t\t %s\n\n Repository:\t\t %s\n\n Source:\t\t %s\n\n Password:\t\t %s\n\n Init_Status:\t\t %s\n\n Exclude List:\t\t %s\n\n""" % (
                    backup_data[row].name, repo_version, backup_data[row].repository, backup_data[row].source, password,
                    backup_data[row].init, backup_data[row].exclude)
            else:
                string = "---------------------------------------------------------------------------------------------------------------------------------------"
                text = """Backup_Name:\t\t %s\n\n Repo Version:\t\t %s\n\n Repository:\t\t %s\n\n Source:\t\t %s\n\n Password:\t\t %s\n\n Init_Status:\t\t %s\n\n Exclude List:\t\t %s\n\n %s \n\nREST:\t\t %s\n\n REST Domain:\t %s\n\n REST Port:\t\t %s\n\n REST User:\t\t %s\n\n REST Password:\t %s\n\n REST Folder:\t\t %s """ % (
                    backup_data[row].name, repo_version, backup_data[row].repository, backup_data[row].source, password,
                    backup_data[row].init, backup_data[row].exclude, string, backup_data[row].rest,
                    backup_data[row].rest_domain, backup_data[row].rest_port, backup_data[row].rest_user, password_rest,
                    backup_data[row].rest_folder)

        self.widget.setPlainText(text)

        self.statusBar().showMessage(backup_data[row].name)

        self.thread_call_repo_version.quit()

    ###############################################
    # Process for call_repo_version when get an error
    ###############################################

    @pyqtSlot(str)
    def call_repo_version_error(self, i):
        """ Signal from worker thread with an error! """

        spinner.stop()

        row = self.listWidget.currentRow()

        msg_box_error(self, i)

        self.widget.setPlainText(i)

        self.statusBar().showMessage((backup_data[row].name))

        self.thread_call_repo_version.quit()

    ###############################################
    # Switch restic <-> restic_dev
    ###############################################

    def restic_switch(self):
        """
        """
        if ini.settings.value('Restic_DEV'):
            if ini.settings.value('Restic_Test_Version') == '1':
                ini.settings.setValue('Restic_Test_Version', '0')
                self.toolbar_restic_test.setText("Restic Version")
                self.toolbar_restic_test.setStyleSheet("color: black ; font-weight: italic ; font-size: 15px")

            else:
                ini.settings.setValue('Restic_Test_Version', '1')
                self.toolbar_restic_test.setText("Restic Dev Version")
                self.toolbar_restic_test.setStyleSheet("color: red ; font-weight: bold ; font-size: 15px")

    ###############################################
    # Close event
    ###############################################

    def close_event(self):
        """ Exit from menu """

        if int(ini.settings.value('Crypt')) == 0:
            sys.exit(0)
        else:
            # Encrypt
            ini.settings.setValue('Crypt', 2)

            # Load key
            key = Crypt.load_key()

            Crypt.encrypt(ini.JSON_FILE, key)

            key = ""
            sys.exit(0)

    ###############################################
    # Load backup data into EditBackupWindow
    ###############################################

    def fill_triggered(self):
        """
        """
        row = ActiveRow.get(self)

        if row == -1:
            msg_box_error(self, "Please selected an data set first!")
            return False
        else:
            self.show_edit_window()
            self.window_edit_backup.input_backup_name.setText(backup_data[row].name)
            self.window_edit_backup.input_repository.setText(backup_data[row].repository)
            self.window_edit_backup.input_source.setText(backup_data[row].source)
            self.window_edit_backup.input_password.setText(backup_data[row].password)
            self.window_edit_backup.input_exclude_list.setText(backup_data[row].exclude)
            self.window_edit_backup.status_init.setText(backup_data[row].init)
            self.window_edit_backup.status_rest.setText(backup_data[row].rest)
            self.window_edit_backup.rest_domain.setText(backup_data[row].rest_domain)
            self.window_edit_backup.rest_port.setText(backup_data[row].rest_port)
            self.window_edit_backup.rest_user.setText(backup_data[row].rest_user)
            self.window_edit_backup.rest_password.setText(backup_data[row].rest_password)
            self.window_edit_backup.rest_folder.setText(backup_data[row].rest_folder)

        if backup_data[row].rest == "1":
            self.window_edit_backup.tab_edit_rest_data.setEnabled(True)
            self.window_edit_backup.input_repository.hide()
            self.window_edit_backup.button_get_repository.hide()

    ###############################################
    # MOUNT
    ###############################################

    def restic_mount(self):
        """ restic -r /home/frank/restic_test/ mount /tmp/restic """

        # We get the current row from the List widget
        row = self.listWidget.currentRow()

        # check for currentRow
        if row == -1:
            msg_box(self, 'Please select a data set first!')
            return False

        # Set name in status bar
        self.statusBar().showMessage(backup_data[row].name)

        # check for INIT
        if backup_data[row].init == "0":
            msg_box_error(self, "Status INIT = 0, please initialize first!")
        else:
            if m_path[0].path:
                msg_box(
                    self, "Mount path is available, you must first umount this repository!")
                return False

            # Do we have Home_Path? When not we will use Home Path from User!
            if not ini.settings.value('Home_Path'):
                home_path = str(Path.home())
            else:
                # use home_path from file settings
                home_path = ini.settings.value('Home_Path')

            # Ask for mount_path
            mount_path = QFileDialog.getExistingDirectory(
                self, "QFileDialog.getOpenFileName()", home_path)

            # set mount_path for use in umount
            # MountPath.set(self, mount_path)
            m_path[0].path = mount_path

            # no passwd in backup_data? Ok, we ask for one
            if not backup_data[row].password:
                pass_word.pw = get_password(self)  # pwd ('1234', True)
                if pass_word.pw[1] is False:
                    return False

            # check if mount_path is set
            if not mount_path:
                msg_box_error(self, "Please select a path!")
                return False

            # we start the worker thread
            self.thread_mount.start()

            # we start waitingspinnerwidget
            spinner.start()

            self.statusBar().showMessage(backup_data[row].name)

    ###############################################
    # UMOUNT
    ###############################################

    def restic_umount(self):
        """ umount PATH """

        # We get the current row from the List widget
        row = self.listWidget.currentRow()

        # get mount_path from MountPath Class object p20
        mount_path = m_path[0].path

        if mount_path == "":
            msg_box(
                self, "No mount path available, you must first mount an repository!")
            return False

        # Set name in status bar
        self.statusBar().showMessage(backup_data[row].name)

        if backup_data[row].init == "0":
            msg_box_error(self, "Status INIT = 0, please initialize first!")

        else:
            try:
                # Create cmd
                cmd = ['umount', mount_path]
                subprocess.Popen(cmd, stdin=subprocess.PIPE)

                msg_box(self, "UMount successful!")
                mainWin.widget.setPlainText("")

            except subprocess.CalledProcessError as error:
                msg_box_error(self, error)

        # After umount, we will delete mount folder!
        try:
            # Delete mount folder
            result = subprocess.run(['rm',
                                     '-rf',
                                     mount_path],
                                    capture_output=True,
                                    text=True)

            # We delete mount_path
            m_path[0].path = ""

        except subprocess.CalledProcessError as error:
            msg_box_error(self, error)

        else:
            pass

        finally:
            self.statusBar().showMessage(backup_data[row].name)
            self.clicked(row)

    ###############################################
    # RESTORE
    ###############################################

    def restic_restore(self):
        """ restic -r /srv/restic-repo restore 79766175 --target /tmp/restore-work """

        # We get the current row from the List widget
        row = self.listWidget.currentRow()

        # We check if a record has been selected.
        if row == -1:
            msg_box(self, 'Please select a data set first!')
            return False

        # Set name in status bar
        self.statusBar().showMessage(backup_data[row].name)

        if backup_data[row].init == "0":
            msg_box_error(self, "Status INIT = 0, please initialize first!")
        else:

            # Do we have Home_Path? When not we will use Home Path from User!
            if not ini.settings.value('Home_Path'):
                home_path = str(Path.home())
            else:
                # use home_path from file settings
                home_path = ini.settings.value('Home_Path')

            res_path.restore = QFileDialog.getExistingDirectory(
                self, "QFileDialog.getOpenFileName()", home_path)

            # no passwd in backup_data? Ok, we ask for one
            if not backup_data[row].password:
                pass_word.pw = get_password(self)  # pwd ('1234', True)
                if pass_word.pw[1] is False:
                    return False

            # check if mount_path is set
            if not res_path.restore:
                msg_box_error(self, "Please select a path!")
                return False

            # msg_box(self, "Process is running")
            # Ask for ID from snapshot
            msg = QInputDialog(self)

            msg.setOkButtonText("OK")
            msg.setCancelButtonText("")

            ID = msg.getText(self, "ID of the snapshot",
                             "ID of the snapshot:", QLineEdit.EchoMode.Normal, "")

            if ID[1] is False:
                self.statusBar().showMessage("")
                return False

            # We loop through the list and output the ID's
            result = False

            for count, value in enumerate(snapshot_id):
                if ID[0] == snapshot_id[count]:
                    result = True
                    snap_id.id1 = ID[0]

            if result is True:
                # inform user
                retval = msg_box_yes_no(
                    "Please wait until the process is finished!", "Runtime Note", self)
                if retval != 16384:
                    self.statusBar().showMessage("")
                    return False

                # we start the worker thread
                self.thread_restore.start()

                # we start waitingspinnerwidget
                spinner.start()

                self.statusBar().showMessage(backup_data[row].name)

            else:
                msg_box_error(self, "Please enter correct ID")
                self.statusBar().showMessage(backup_data[row].name)
                mainWin.widget.setPlainText("")
                self.clicked(row)
                return False

    ###############################################
    # CHECK
    ###############################################

    def restic_check(self):
        """ restic -r /home/frank/restic_test/ check """

        # We get the current row from the List widget
        row = self.listWidget.currentRow()

        # We check if a record has been selected.
        if row == -1:
            msg_box(self, 'Please select a data set first!')
            return False

        # We check if the dataset has already been initialized.
        if backup_data[row].init == '1':

            # no passwd in backup_data? Ok, we ask for one
            if not backup_data[row].password:
                pass_word.pw = get_password(self)  # pwd ('1234', True)
                if pass_word.pw[1] is False:
                    return False

            self.statusBar().showMessage("Process is running...")

            # inform user
            retval = msg_box_yes_no(
                "Please wait until the process is finished!", "Runtime Note", self)
            # print(retval)
            if retval != 16384:
                self.statusBar().showMessage("")
                return False

            # we start the worker thread
            self.thread_check.start()

            # we start waitingspinnerwidget
            spinner.start()

            self.statusBar().showMessage(backup_data[row].name)

        else:
            msg_box_error(self, "Status INIT = 0, please initialize first!")
            return False

    ###############################################
    # migrate CHECK
    ###############################################

    def restic_migrate_check(self):
        """ restic migrate -r /home/frank/restic_test/ """

        # We get the current row from the List widget
        row = self.listWidget.currentRow()

        # We check if a record has been selected.
        if row == -1:
            msg_box(self, 'Please select a data set first!')
            return False

        # We check if the dataset has already been initialized.
        if backup_data[row].init == '1':

            # no passwd in backup_data? Ok, we ask for one
            if not backup_data[row].password:
                pass_word.pw = get_password(self)  # pwd ('1234', True)
                if pass_word.pw[1] is False:
                    return False

            self.statusBar().showMessage("Process is running...")

            # inform user
            retval = msg_box_yes_no(
                "Please wait until the process is finished!", "Runtime Note", self)
            # print(retval)
            if retval != 16384:
                self.statusBar().showMessage("")
                return False

            # we start the worker thread
            self.thread_migrate_check.start()

            # we start waitingspinnerwidget
            spinner.start()

            self.statusBar().showMessage(backup_data[row].name)

        else:
            msg_box_error(self, "Status INIT = 0, please initialize first!")
            return False

    ###############################################
    # migrate update
    ###############################################

    def restic_migrate_update(self):
        """ restic migrate -r /home/frank/restic_test/ """

        # We get the current row from the List widget
        row = self.listWidget.currentRow()

        # We check if a record has been selected.
        if row == -1:
            msg_box(self, 'Please select a data set first!')
            return False

        # We check if the dataset has already been initialized.
        if backup_data[row].init == '1':

            # no passwd in backup_data? Ok, we ask for one
            if not backup_data[row].password:
                pass_word.pw = get_password(self)  # pwd ('1234', True)
                if pass_word.pw[1] is False:
                    return False

            self.statusBar().showMessage("Process is running...")

            # inform user
            retval = msg_box_yes_no(
                "Please wait until the process is finished!", "Runtime Note", self)
            # print(retval)
            if retval != 16384:
                self.statusBar().showMessage("")
                return False

            # we start the worker thread
            self.thread_migrate_update.start()

            # we start waitingspinnerwidget
            spinner.start()

            self.statusBar().showMessage(backup_data[row].name)

        else:
            msg_box_error(self, "Status INIT = 0, please initialize first!")
            return False

    ###############################################
    # CACHE --cleanup
    ###############################################

    def restic_cache(self):
        """ restic -r /home/frank/restic_test/ cache --cleanup """

        # We get the current row from the List widget
        row = self.listWidget.currentRow()

        # We check if a record has been selected.
        if row == -1:
            msg_box(self, 'Please select a data set first!')
            return False

        # We check if the dataset has already been initialized.
        if backup_data[row].init == '1':

            # no passwd in backup_data? Ok, we ask for one
            if not backup_data[row].password:
                pass_word.pw = get_password(self)  # pwd ('1234', True)
                if pass_word.pw[1] is False:
                    return False

            self.statusBar().showMessage("Process is running...")

            # inform user
            retval = msg_box_yes_no(
                "Please wait until the process is finished!", "Runtime Note", self)
            # print(retval)
            if retval != 16384:
                self.statusBar().showMessage("")
                return False

            # we start the worker thread
            self.thread_cache.start()

            # we start waitingspinnerwidget
            spinner.start()

            self.statusBar().showMessage(backup_data[row].name)

        else:
            msg_box_error(self, "Status INIT = 0, please initialize first!")
            return False

    ###############################################
    # STATS
    ###############################################

    def restic_stats(self):
        """ restic -r /home/frank/restic_test/ stats """

        # We get the current row from the List widget
        row = ActiveRow.get(self)

        # We check if a record has been selected.
        if row == -1:
            msg_box(self, 'Please select a data set first!')
            return False

        # no passwd in backup_data? Ok, we ask for one
        if not backup_data[row].password:
            pass_word.pw = get_password(self)  # pwd ('1234', True)
            if pass_word.pw[1] is False:
                return False

        # We check if the dataset has already been initialized.
        match backup_data[row].init:

            case '0':
                msg_box_error(self, "Status INIT = 0, please initialize first!")
                return False

            case '1':
                # inform user
                retval = msg_box_yes_no(
                    "Please wait until the process is finished!", "Runtime Note", self)
                if retval != 16384:
                    self.statusBar().showMessage("")
                    return False

                # we start the worker thread
                self.thread_stats.start()

                # we start waitingspinnerwidget
                spinner.start()

                self.statusBar().showMessage(backup_data[row].name)

            case _:
                msg_box_error(self, "Pattern match not found")
                return False

    ###############################################
    # VERSION
    ###############################################

    def restic_version(self):
        """ restic version """

        restic = ResticDev.get(self)

        try:
            process = subprocess.run([restic, 'version'],
                                     capture_output=True,
                                     text=True)
            process.check_returncode()

            mainWin.widget.setPlainText(process.stdout)
        except subprocess.CalledProcessError as error:
            msg_box_error(self, error)

        self.statusBar().showMessage(("Restic version"))

    ###############################################
    # PRUNE
    ###############################################

    def restic_prune(self):
        """ restic -r /home/frank/restic_test/ forget --keep-last 3 --keep-monthly 3 --prune """

        # We get the current row from the List widget
        row = self.listWidget.currentRow()

        # We check if a record has been selected.
        if row == -1:
            msg_box(self, 'Please select a data set first!')
            return False

        # no passwd in backup_data? Ok, we ask for one
        if not backup_data[row].password:
            pass_word.pw = get_password(self)  # pwd ('1234', True)
            if pass_word.pw[1] is False:
                return False

        self.statusBar().showMessage("Process is running...")

        # We check if the dataset has already been initialized.
        if backup_data[row].init == '1':

            # inform user
            retval = msg_box_yes_no(
                "Please wait until the process is finished!", "Runtime Note", self)
            if retval != 16384:
                self.statusBar().showMessage("")
                return False

            # we start the worker thread
            self.thread_prune.start()

            # we start waitingspinnerwidget
            spinner.start()

            self.statusBar().showMessage(backup_data[row].name)

        else:
            msg_box_error(self, "Status INIT = 0, please initialize first!")
            return False

    ###############################################
    # SNAPSHOTS
    ###############################################

    def restic_snapshots(self):
        """ restic -r /home/frank/restic_test/ snapshots """

        # If the list snapshot_id is not exactly 1, then delete!
        SnapshotID.id_remove(self)

        # We get the current row from the List widget
        row = self.listWidget.currentRow()

        # We check if a record has been selected.
        if row == -1:
            msg_box(self, 'Please select a data set first!')
            status.status_snapshots = 1
            return False

        self.statusBar().showMessage("Process is running...")

        # We check if the dataset has already been initialized.
        if backup_data[row].init == '1':

            # no passwd in backup_data? Ok, we ask for one
            if not backup_data[row].password:
                pass_word.pw = get_password(self)  # pwd ('1234', True)
                if pass_word.pw[1] is False:
                    return False

            # inform user
            retval = msg_box_yes_no(
                "Please wait until the process is finished!", "Runtime Note", self)
            if retval != 16384:
                self.statusBar().showMessage("")
                status.status_snapshots = 1
                return False

            # we start the worker thread
            self.thread_snapshots.start()

            # we start waitingspinnerwidget
            spinner.start()

            self.statusBar().showMessage(backup_data[row].name)

        else:
            msg_box_error(self, "Status INIT = 0, please initialize first!")
            return False

    ###############################################
    # Menu Tools/Snapshots triggered
    ###############################################

    def menu_restic_snapshots_triggered(self):
        """ Set start parameter .isEnabled for menu Tools/Snapshots and Tools/List Snapshot """

        if status.status_snapshots == 1:
            self.restic_ls.setEnabled(False)
            self.restic_list.setEnabled(True)
            status.status_snapshots = 0
        else:
            self.restic_ls.setEnabled(True)
            self.restic_list.setEnabled(False)

    ###############################################
    # LS SNAPSHOT-ID
    ###############################################

    def restic_ls2(self):
        """ restic -r /home/frank/restic_test/ ls <ID> """

        row = self.listWidget.currentRow()

        self.statusBar().showMessage("Process is running...")

        # no passwd in backup_data? Ok, we ask for one
        if not backup_data[row].password:
            pass_word.pw = get_password(self)  # pwd ('1234', True)
            if pass_word.pw[1] is False:
                return False

        # We check if the dataset has already been initialized.
        if backup_data[row].init == '1':

            # msg_box(self, "Process is running")
            # Ask for ID from snapshot
            msg = QInputDialog(self)
            msg.setOkButtonText("OK")
            msg.setCancelButtonText("")

            ID = msg.getText(self, "ID of the snapshot",
                             "ID of the snapshot:", QLineEdit.EchoMode.Normal, "")

            # We loop through the list and output the ID's
            result = False

            for count, value in enumerate(snapshot_id):
                if ID[0] == snapshot_id[count]:
                    result = True
                    snap_id.id1 = ID[0]

            # if ID[0] != "" and ID[1] is True:
            # If the input is correct, we continue with the normal function
            if result is True:
                # inform user
                retval = msg_box_yes_no(
                    "Please wait until the process is finished!", "Runtime Note", self)

                if retval != 16384:
                    self.statusBar().showMessage("")
                    return False

                # we start the worker thread
                self.thread_list_snapshot.start()

                # we start waitingspinnerwidget
                spinner.start()

                self.statusBar().showMessage(backup_data[row].name)

                # Processing the subprocess.run

            else:
                msg_box_error(self, "Please enter correct ID")
                self.statusBar().showMessage(backup_data[row].name)
                mainWin.widget.setPlainText("")
                self.clicked(row)
                return False

        else:
            msg_box_error(self, "Status INIT = 0, please initialize first!")
            return False

    ###############################################
    # Menu Tools/List snapshot triggered
    ###############################################

    def menu_restic_ls2_triggered(self):
        """ Set start parameter .isEnabled for menu Tools/Snapshots and Tools/List Snapshot """

        self.restic_ls.setEnabled(False)
        self.restic_list.setEnabled(True)

    ###############################################
    # INIT
    ###############################################

    def restic_init(self):
        """ restic -r /home/frank/restic_test/ init """

        BackupList.make_key_list_new(self)

        # We get the current row from the List widget
        row = self.listWidget.currentRow()

        # We check if a record has been selected.
        if row == -1:
            msg_box(self, 'Please select a data set first!')
            return False

        # no passwd in backup_data? Ok, we ask for one
        if not backup_data[row].password:
            pass_word.pw = get_password(self)  # pwd ('1234', True)
            if pass_word.pw[1] is False:
                return False

        self.statusBar().showMessage("Process is running...")

        # We check if the dataset has already been initialized.
        if backup_data[row].init == '0':
            # inform user
            retval = msg_box_yes_no(
                "Please wait until the process is finished!", "Runtime Note", self)
            if retval != 16384:
                self.statusBar().showMessage("")
                return False
        else:
            msg_box(self, "Initialization already present!")
            return False

        # we start the worker thread
        self.thread_init.start()

        # we start waitingspinnerwidget
        spinner.start()

        self.statusBar().showMessage(backup_data[row].name)

    ###############################################
    # BACKUP
    ###############################################

    def restic_backup(self):
        """ restic -r /home/frank/restic_test/ backup /home/frank/Picture --exclude-file=excludes.txt """

        # We get the current row from the List widget
        row = self.listWidget.currentRow()

        # We check if a record has been selected.
        if row == -1:
            msg_box(self, 'Please select a data set first!')
            return False

        if backup_data[row].rest == "0":
            REPO_CONFIG_FILE = f"{backup_data[row].repository}/config"
            if not Path(REPO_CONFIG_FILE).exists():
                msg_box_error(self, "Repository is not accessible. Drive mounted?")
                return

        # no passwd in backup_data? Ok, we ask for one
        if not backup_data[row].password:
            pass_word.pw = get_password(self)  # pwd ('1234', True)
            if pass_word.pw[1] is False:
                return False

        self.statusBar().showMessage("Process is running...")

        # We check if the dataset has already been initialized.
        # if backup_data[row].init == '1':
        match backup_data[row].init:

            case "1":
                # inform user
                retval = msg_box_yes_no(
                    "Please wait until the process is finished!", "Runtime Note", self)
                if retval != 16384:
                    self.statusBar().showMessage("")
                    return False

                # we start the worker thread
                self.thread_backup.start()

                # we start waitingspinnerwidget
                spinner.start()

                self.statusBar().showMessage(backup_data[row].name)

            case "0":
                msg_box_error(self, "Status INIT = 0, please initialize first!")
                return False

            case _:
                msg_box_error(self, "Pattern match not found")
                return False


    ###############################################
    # UNLOCK
    ###############################################

    def restic_unlock(self):
        """ restic -r /home/frank/restic_test/ unlock """

        # We get the current row from the List widget
        row = self.listWidget.currentRow()

        # We check if a record has been selected.
        if row == -1:
            msg_box(self, 'Please select a data set first!')
            return False

        # no passwd in backup_data? Ok, we ask for one
        if not backup_data[row].password:
            pass_word.pw = get_password(self)  # pwd ('1234', True)
            if pass_word.pw[1] is False:
                return False

        self.statusBar().showMessage("Process is running...")

        # We check if the dataset has already been initialized.
        if backup_data[row].init == '1':

            # inform user
            retval = msg_box_yes_no(
                "Please wait until the process is finished!", "Runtime Note", self)
            if retval != 16384:
                self.statusBar().showMessage("")
                return False

            # we start the worker thread
            self.thread_unlock.start()

            # we start waitingspinnerwidget
            spinner.start()

            self.statusBar().showMessage(backup_data[row].name)

        else:
            msg_box_error(self, "Status INIT = 0, please initialize first!")
            return False

    ###############################################
    # Evaluation click listWidget
    ###############################################

    def clicked(self, item):
        """
        """
        with open(Path(ini.JSON_FILE), 'r', encoding='utf-8') as f:
            # with open('backup_list.json', 'r', encoding='utf-8') as f:
            backups = json.load(f)

        keys_list = []

        for key in backups:
            keys_list.append(key)

        for count, values in enumerate(keys_list):
            backup_data[count] = BackupList(backups[keys_list[count]]['name'],
                                            backups[keys_list[count]]['repository'],
                                            backups[keys_list[count]]['source'],
                                            backups[keys_list[count]]['password'],
                                            backups[keys_list[count]]['init'],
                                            backups[keys_list[count]]['exclude'],
                                            backups[keys_list[count]]['rest'],
                                            backups[keys_list[count]]['rest_domain'],
                                            backups[keys_list[count]]['rest_port'],
                                            backups[keys_list[count]]['rest_user'],
                                            backups[keys_list[count]]['rest_password'],
                                            backups[keys_list[count]]['rest_folder'])

        # we start the worker thread
        mainWin.thread_call_repo_version.start()

        # we start waitingspinnerwidget
        spinner.start()

        # Set start parameter .isEnabled for menu Tools/Snapshots and Tools/List Snapshot.
        self.restic_ls.setEnabled(False)
        self.restic_list.setEnabled(True)

    ###############################################
    # Del Data
    ###############################################

    def delete_data(self):
        """ We delete the data of the backup """

        # We get the current row from the List widget
        row = self.listWidget.currentRow()

        try:
            # Restic function
            result = subprocess.run(['rm',
                                     '-rf',
                                     backup_data[row].repository],
                                    capture_output=True,
                                    text=True)

        except subprocess.CalledProcessError as error:
            msg_box_error(self, error)

        else:
            pass

        finally:
            pass

    ###############################################
    # Del Entry
    ###############################################

    def del_entry(self):
        """ Del entry """

        row = self.listWidget.currentRow()
        # check for loaded data

        if row == -1:
            msg_box(self, 'Please select a data set first!')
            return False
        else:
            if backup_data[row].rest == "1":
                retval = msg_box_yes_no(
                    'Really delete entry?', 'Delete entry?', self)
                if retval == 16384:
                    msg_box_error(
                        self, "Backup is on the REST server, delete the data on server manually!")
                    BackupList.del_entry(self, row)
                    # row = -1
                    return False
                else:
                    return False

            else:
                retval = msg_box_yes_no(
                    'Really delete entry and backup data?', 'Delete data?', self)
                if retval == 16384:
                    self.delete_data()
                    BackupList.del_entry(self, row)
                    # row = -1
                else:
                    return False

    #####################
    # Menu Help
    #####################

    def button_about(self):
        """ About button """

        about_message = f"<b>Restic UI</b><br>My second python3 project ;)<br>Version {ini.settings.value('version')} <br>© Frank Mankel 2020 - 2022<br><a href='https://linux-nerds.org'>https://linux-nerds.org/</a><br><br>This project is not related to <a href='https://restic.net/'>https://restic.net/</a><br><br>Thanks for this nice tool!<br><a href='https://restic.net/'>https://restic.net/</a><br><br>Logo is &#169; <a href='https://restic.net/'>https://restic.net/</a>"

        msg_box_about(about_message, self)

    #####################
    # Call Add Backup Window
    #####################

    def show_add_window(self):
        """ Call Add Backup Window """
        self.window_add_backup.show()

    #####################
    # Call Edit Backup Window
    #####################

    def show_edit_window(self):
        """ Call Edit Backup Window """
        self.window_edit_backup.show()

    #####################
    # Call Setup Window
    #####################

    def show_settings_window(self):
        """ Call Setup Window """

        settings_window = SettingsWindow()
        settings_window.send_signal.connect(self.change_toolbar)
        settings_window.show()

    #####################
    # Manipulate toolbar from settings window
    #####################

    def change_toolbar(self, restic_version):
        """ Manipulate toolbar from settings window """

        if restic_version == "Restic DEV Version":
            self.toolbar_restic_test.setText("Restic Dev Version")
            self.toolbar_restic_test.setStyleSheet("color: red ; font-weight: bold ; font-size: 15px")
            ini.settings.setValue('Restic_Test_Version', '1')

        else:
            self.toolbar_restic_test.setText("Restic Version")
            self.toolbar_restic_test.setStyleSheet("color: black ; font-weight: italic ; font-size: 15px")
            ini.settings.setValue('Restic_Test_Version', '0')


#####################
# Send push notification to desktop
#####################


def push(title, message):
    """ Send push notification to desktop (only linux) """

    if os.name == "posix":
        command = f'''
        notify-send "{title}" "{message}"
        '''
    else:
        return

    os.system(command)


###############################################
# Program Loop
###############################################


if __name__ == "__main__":

    app = QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.show()
    spinner = QtWaitingSpinner(mainWin, True, True, Qt.WindowModality.ApplicationModal)
    app.aboutToQuit.connect(mainWin.close_event)
    app.exec()
