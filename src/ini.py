"Used for initialization of the program"

# ------ import for logging ------ #
import logging

# ------ import for file check ------ #
from pathlib import Path

from PyQt6.QtCore import QSettings

###############################################
# Set settings
###############################################

settings = QSettings("FrankMankel", "Restic_UI")

if settings.contains('Crypt'):
    pass
else:
    settings.setValue('Exclude_List', "")
    settings.setValue('Home_Path', "")
    settings.setValue('Key_Path', "")
    settings.setValue('Source_Path', "")
    settings.setValue('Crypt', 0)
    settings.setValue('Key', 0)
    settings.setValue('row', -1)
    settings.setValue("version", '1.5.1')
    settings.setValue("Restic_Check_Percent", '75')
    settings.setValue("Restic_Keep_Last", '3')
    settings.setValue("Restic_Keep_Monthly", '3')
    settings.setValue("Restic_Test_Version", '0')

if settings.value('version') == "1.5.0":
    settings.setValue("version", '1.5.1')

###############################################
# Set paths and file names
###############################################

# set Current working directory(CWD)
CWD = str(Path.cwd())

# Get home directory from user
USERHOME = str(Path.home())

###############################################
# Check and create .restic_ui & .restic_ui/log.txt
###############################################

# Create ~/.restic_ui if not exists
PATH_RESTIC_UI = Path(f"{USERHOME}/.restic_ui")
PATH_RESTIC_UI.mkdir(parents=True, exist_ok=True)

file_name = Path(f"{USERHOME}/.restic_ui/log.txt")
file_name.touch(exist_ok=True)

###############################################
# Logging
###############################################

# Set logging config
logging.basicConfig(filename=f"{USERHOME}/.restic_ui/log.txt",
                    format='%(asctime)s %(levelname)s:%(message)s',
                    level=logging.DEBUG,
                    datefmt='%d.%m.%Y %H:%M:%S')

###############################################
# Set paths and file names
###############################################

# Set name for JSON_FILE
FILE_BACKUP_LIST = 'backup_list.json'  # set filename

# set path & filenames for crypt function
JSON_FILE = f"{USERHOME}/.restic_ui/{FILE_BACKUP_LIST}"

CRYPT_FILE = f"{USERHOME}/.restic_ui/backup_list.enc"

KEY_PATH = settings.value("Key_Path")

KEY_FILE = f"{KEY_PATH}/crypt.key"
