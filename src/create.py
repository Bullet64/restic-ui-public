"Create backup_list.json if not exists"

# ------ import for file check ------ #
from pathlib import Path

# ------ import for json ------ #
import json

import ini


# we build backup_list.json
class CreateFile():

    """I build here a new class to manage the backup list.
    Data stored in backup_list.json"""

    def __init__(self,
                 ex_name,
                 ex_repository,
                 ex_source,
                 ex_password,
                 ex_init,
                 ex_exclude,
                 ex_rest,
                 ex_rest_domain,
                 ex_rest_port,
                 ex_rest_user,
                 ex_rest_password,
                 ex_rest_folder):
        self.ex_name = ex_name
        self.ex_repository = ex_repository
        self.ex_source = ex_source
        self.ex_password = ex_password
        self.ex_init = ex_init
        self.ex_exclude = ex_exclude
        self.ex_rest = ex_rest
        self.ex_rest_domain = ex_rest_domain
        self.ex_rest_port = ex_rest_port
        self.ex_rest_user = ex_rest_user
        self.ex_rest_password = ex_rest_password
        self.ex_rest_folder = ex_rest_folder


p1 = CreateFile("Example1",
                str(Path.home()) + "/backup1",
                str(Path.home()) + "/source1",
                "1234",
                "0",
                "",
                "0",
                "",
                "",
                "",
                "",
                "")

p2 = CreateFile("Example2",
                str(Path.home()) + "/backup2",
                str(Path.home()) + "/source2",
                "123456",
                "0",
                "",
                "0",
                "",
                "",
                "",
                "",
                "")

p3 = CreateFile("Example3",
                str(Path.home()) + "/backup3",
                str(Path.home()) + "/source3",
                "12345678",
                "0",
                "",
                "1",
                "",
                "",
                "",
                "",
                "")

example_list = {}
example_list['0'] = {'name': p1.ex_name,
                     'repository': p1.ex_repository,
                     'source': p1.ex_source,
                     'password': p1.ex_password,
                     'init': p1.ex_init,
                     'exclude': p1.ex_exclude,
                     'rest': p1.ex_rest,
                     'rest_domain': p1.ex_rest_domain,
                     'rest_port': p1.ex_rest_port,
                     'rest_user': p1.ex_rest_user,
                     'rest_password': p1.ex_rest_password,
                     'rest_folder': p1.ex_rest_folder}
example_list['1'] = {'name': p2.ex_name,
                     'repository': p2.ex_repository,
                     'source': p2.ex_source,
                     'password': p2.ex_password,
                     'init': p2.ex_init,
                     'exclude': p2.ex_exclude,
                     'rest': p2.ex_rest,
                     'rest_domain': p2.ex_rest_domain,
                     'rest_port': p2.ex_rest_port,
                     'rest_user': p2.ex_rest_user,
                     'rest_password': p2.ex_rest_password,
                     'rest_folder': p2.ex_rest_folder}
example_list['2'] = {'name': p3.ex_name,
                     'repository': p3.ex_repository,
                     'source': p3.ex_source,
                     'password': p3.ex_password,
                     'init': p3.ex_init,
                     'exclude': p3.ex_exclude,
                     'rest': p3.ex_rest,
                     'rest_domain': p3.ex_rest_domain,
                     'rest_port': p3.ex_rest_port,
                     'rest_user': p3.ex_rest_user,
                     'rest_password': p3.ex_rest_password,
                     'rest_folder': p3.ex_rest_folder}

# we save backup_list.json
try:
    with open(Path(ini.JSON_FILE), 'w', encoding='utf8') as f:
        json.dump(example_list, f)
except Exception:
    print("An error has occurred")
