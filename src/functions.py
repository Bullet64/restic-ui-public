"""my functions"""

# ------ import for file check ------ #
from pathlib import Path

# ------ import for PyQt6 ------ #
from PyQt6.QtWidgets import QMessageBox, QFileDialog, QInputDialog, QLineEdit

from PyQt6.QtCore import QSettings

from PyQt6.QtGui import QPixmap

import ini

settings = QSettings("FrankMankel", "Restic_UI")

# ----functions ---- #


def check_if_file_exists(file, self):
    """ Check if file exists """
    if file.is_file():
        # file exists
        msg = QMessageBox(self)
        msg.setWindowTitle("File exists")
        msg.setText(f"File exists\n {file}")
        msg.setIcon(msg.Information)
        retval = msg.exec()
        return(retval)
    else:
        msg = QMessageBox(self)
        msg.setWindowTitle("File don't exists")
        msg.setText("File don't exists")
        msg.setInformativeText("OK will create it!")
        msg.setIcon(msg.Critical)
        msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        retval = msg.exec()
        return(retval)

# ---- Message Boxes ---- #


def msg_box(self, message):
    """ Message Box """
    msg = QMessageBox(self)
    msg.setIcon(QMessageBox.Icon.Information)
    msg.setWindowTitle("Message")
    msg.setText(message)
    msg.exec()


def msg_box_about(message, self):
    """ Message Box About """
    image = QPixmap(f'{ini.USERHOME}/restic-ui-public/icons/Restic_Logo.png')
    msg = QMessageBox(self)
    msg.setIcon(QMessageBox.Icon.Information)
    msg.setIconPixmap(image)
    msg.setWindowTitle("Message")
    msg.setText(message)
    msg.exec()


def msg_box_yes_no(message, message2, self):
    """ Message Box Yes / No """
    msg = QMessageBox(self)
    msg.setStandardButtons(QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No)
    msg.setWindowTitle(message2)
    msg.setText(message)
    msg.setDefaultButton(QMessageBox.StandardButton.Yes)

    if msg == QMessageBox.close:
        retval = -1
    else:
        retval = msg.exec()
    return(retval)


def msg_box_error(self, error_message):
    """ Message Box Error """
    msg = QMessageBox(self)
    msg.setIcon(QMessageBox.Icon.Critical)
    msg.setWindowTitle("Error Message")
    msg.setText(error_message)
    msg.exec()


def get_password(self):
    """ Get password for repository """
    # Ask for ID from snapshot
    msg = QInputDialog(self)
    passwd = msg.getText(self, "Password of repository",
                         "Password of repository:", QLineEdit.EchoMode.Normal, "")
    return passwd


def get_path(self, key_word, text):
    """ Get path """
    # Does the file already exist?

    if ini.settings.value(key_word) != "":
        # Yes, is available! We get the path from the settings!
        dir_path = QFileDialog.getExistingDirectory(
            self, f"{text}", ini.settings.value(key_word))
    else:
        # No, we are using home folder from user.
        home_path3 = (Path.home())
        dir_path = QFileDialog.getExistingDirectory(
            self, f"{text}", str(home_path3))
    return dir_path


def get_file_path(self, key_word, text):
    """ Get file path """
    # Does the file already exist?
    if ini.settings.value(key_word) != "":
        # Yes, is available! We get the path from the settings!
        file_path = QFileDialog.getOpenFileName(
            self, f"{text}", ini.settings.value(key_word))
    else:
        # No, we are using home folder from user.
        home_path3 = (Path.home())
        file_path = QFileDialog.getOpenFileName(
            self, f"{text}", str(home_path3))
    return file_path
